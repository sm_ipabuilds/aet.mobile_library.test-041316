# README #

ScrollMotion MobileLibrary front-end Content Spec and JavaScript in the **master** branch is production ready. Bleeding-edge development can be found in the **dev** branch and may be unstable at times and subject to change.

### What is this repository for? ###

* ScrollMotion MobileLibrary front-end developed in Content Spec and JavaScript

### How can you contribute? ###

* Feature requests should be sent to support@scrollmotion.com
* Anyone with write access is free to fork branches but please be specific in branch description and all commit descriptions
* If you do not have write access but would like to, please contact Jon May
* Pull requests may be accepted into **dev** during normal sprint cycles and are subject to refusal if it fails to meet criteria

### Who do I talk to? ###

If you have any questions or if you are experiencing any issues please contact one of the following contributors:

* Jon May
* Lucas Nguyen
* Paul Seltmann
* Michael Seltmann