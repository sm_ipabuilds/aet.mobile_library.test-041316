// Custom Javascript
// Created by Jon May 2.1.2013
// Expanded by James Granata 10.1.2013

/* global borg, SM */
// This needs to be cleaned up

var ISDEBUG = true;

var SEP = ( function( SEP ) {

    /*
        * @desc Debug wrapper prints params to console or alert
        * @param string, array, boolean, object, function - an indefinite number of params are accepted
        * @param boolean - last parameter will alert params if true
        * @return undefined
        * @required window.ISDEBUG - value must be true to print to console.
        * @required SM
    */
    function debug() {
        var args = arguments;

        if( window.ISDEBUG ) {
            if( args.length > 1 && args[ 0 ] === true ) {
                SM.alert( Array.prototype.slice.call( args ).slice( 1 ) );
            } else if( SM.log ) {
                // SM.log appears to clip content printed to console
                // Example, when printing the viewHistory Array
                SM.log( Array.prototype.slice.call( args ) );
            }
        }
    }

    function timer( id ) {
        timer.timers = timer.timers || {};
        timer.id = timer.id || 0;
        if( id && timer.timers[ id ] ) { return new Date().getTime() - timer.timers[ id ]; }
        timer.timers[ ++timer.id ] = new Date().getTime();
        return timer.id;
    }

    /*
        * @desc bind onChange event to textfield to capture 
        * user inputted text
        * @param string textField - textfield overlayId
        * @return undefined
        * @required borg
    */
    function captureEmail( textField ) {
        borg.getOverlayById( textField ).onChange = function( data ) {
            borg.setPersistentItem( "SEP.EULA.deviceUser" , data.text );
        };
    }

    /*
        * @desc set greeting string to text overlay based on time of day  
        * @param string morningMsg - message for morning hours
        * @param string afternoonMsg - message for afternoon hours
        * @param string eveningMsg - message for evening hours
        * @param string defaultMsg - default message
        * @return undefined
        * @required SM
    */
    function timeOfDay( morningMsg , afternoonMsg , eveningMsg , defaultMsg , overlayMsg ) {
        var now = new Date(),
            hours = now.getHours(),
            msg;

        if( hours >= 3 && hours < 12 ) {
            msg = morningMsg;
        } else if( hours >= 12 && hours < 17 ) {
            msg = afternoonMsg;
        } else if( ( hours < 3 ) || ( hours >= 17 && hours <= 24 ) ) {
            msg = eveningMsg;
        } else {
            msg = defaultMsg;
        }

        SM.setText( overlayMsg , msg );
    }

    function logInWrongCredentials() {
        SM.runAction( {
            "action": "displayAlertAction",
            "target": "alert_plugin",
            "source": "#keyValueStore",
            "trigger": "now",
            "data": {
                "title": "",
                "body": SEP.updateTranslationForString( arguments[ 0 ] ),
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "OK" ),
                        "actions": [
                            {
                                "action": "setTextAction",
                                "trigger": "now",
                                "target": "PasswordTxt",
                                "data": {
                                    "text": ""
                                }
                            },
                            {
                                "action": "setPassword",
                                "trigger": "now",
                                "target": "#systemAuthentication",
                                "data": {
                                    "password": ""
                                }
                            },
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            },
                            {
                                "action": "closeKeyboardAction",
                                "trigger": "now",
                                "targets": [
                                    "UserNameTxt",
                                    "PasswordTxt"
                                ]
                            }
                        ]
                    }
                ]
            }
        } );
    }
    function invalidCertificate() {
        SM.runAction( {
            "action": "displayAlertAction",
            "target": "alert_plugin",
            "source": "#keyValueStore",
            "trigger": "now",
            "data": {
                "title": "",
                "body": SEP.updateTranslationForString( arguments[ 0 ] ),
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "OK" ),
                        "actions": [
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            }
                        ]
                    }
                ]
            }
        } );
    }
    function hostConnectionError() {
        SM.runAction( {
            "action": "displayAlertAction",
            "source": "#keyValueStore",
            "trigger": "now",
            "target": "alert_plugin",
            "data": {
                "title": "",
                "body": SEP.updateTranslationForString( arguments[ 0 ] ),
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "OK" ),
                        "actions": [
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            }
                        ]
                    }
                ]
            }
        } );
    }
    function noEntitledPackages() {
        SM.runAction( {
            "action": "displayAlertAction",
            "target": "alert_plugin",
            "source": "#keyValueStore",
            "trigger": "now",
            "data": {
                "title": "",
                "body": SEP.updateTranslationForString( arguments[ 0 ] ),
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "OK" ),
                        "actions": [
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            }
                        ]
                    }
                ]
            }
        } );
    }
    function forceUpdate() {
        var packageID = SM.getProperty("#systemPackageDataRegistry", "packageID");
        
        if (packageID != SEP.filereaderPackage) {
            SM.runAction( {
                "action": "displayAlertAction",
                "source": "#systemPackageDataRegistry",
                "target": "alert_plugin",
                "trigger": "now",
                "data": {
                    "title": "#(title)",
                    "body": SEP.updateTranslationForString( "An updated version has been automatically downloaded and is available on your device." ),
                    "buttons": [
                        {
                            "text": SEP.updateTranslationForString( "OK" ),
                            "actions": [
                                {
                                    "action": "dismissAlertAction",
                                    "trigger": "touchUpInside",
                                    "target": "alert_plugin"
                                }
                            ]
                        }
                    ]
                }
            } );
        }
    }
    function updateAvailable() {
        SM.runAction( {
            "action": "displayAlertAction",
            "source": "#systemPackageDataRegistry",
            "target": "alert_plugin",
            "trigger": "now",
            "data": {
                "title": "#(title)",
                "body": SEP.updateTranslationForString( "An automatic version has been automatically downloaded. Would you like to install it now?" ),
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "Install Now" ),
                        "actions": [
                            {
                                "action": "installPackage",
                                "trigger": "touchUpInside",
                                "source": "#systemPackageDataRegistry",
                                "target": "#systemPackageDataRegistry",
                                "data": {
                                    "packageID": "#packageID"
                                }
                            },
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            }
                        ]
                    },
                    {
                        "text": SEP.updateTranslationForString( "Cancel" ),
                        "actions": [
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            }
                        ]
                    }
                ]
            }
        } );
    }

    function startService( id ) {
        startService.setInterval = startService.setInterval || false;
        SEP.debug( "startService.setInterval: " + startService.setInterval );

        if( id === "downloadPackageMetaData" && startService.setInterval ) {
            return;
        } else {
            startService.setInterval = true;
        }
        SM.runAction( {
            "action": "startService",
            "trigger": "now",
            "target": "#serviceController",
            "data": {
                "serviceID": id
            }
        } );
    }

    /*
        * @desc Convenience function for getWebKeyValueStore. This is required because
            Multiple subscriptions to the same event will fail with identical event + action + target
        * @param string - an indefinite number of params are accepted
        * @return undefined
        * @required SM
    */
    function getWebKeyValueStore() {
        var keys = Array.prototype.slice.call( arguments ),
            i = 0,
            len = keys.length;

        SEP.debug( "Retrieving keys from WC: " + keys );
        for( i ; i < len ; i++ ) {
            SM.runAction( {
                "action": "getWebKeyValueStore",
                "trigger": "now",
                "source": "#keyValueStore",
                "target": "#keyValueStore",
                "data": {
                    "url": keys[ i ]
                }
            } );
        }
    }

    /*
        * @desc Convenience function for contact support button. This will present a wifi message if offline
        * @return undefined
        * @required SM
    */
    function contactSupport() {
        SM.runAction( {
            "action": "#spawnOnce",
            "trigger": "now",
            "data": {
                "overlayId": "support_email"
            }
        } );
    }
    
    /*
        * @desc Convenience function for send feedback button. This will present a wifi message if offline
        * @return undefined
        * @required SM
    */
    function sendFeedback() {
        SM.runAction( {
            "action": "#spawnOnce",
            "trigger": "now",
            "data": {
                "overlayId": "feedback_email"
            }
        } );
    }       
    
    function offlineAlert() {
        SM.runAction( {
            "action": "displayAlertAction",
            "target": "alert_plugin",
            "trigger": "now",
            "data": {
                "title": SEP.updateTranslationForString( "You are not connected to the internet." ),
                "body": SEP.updateTranslationForString( "Please connect to the internet and try again." ),
                "image": "art_assets/noWiFi.png",
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "OK" ),
                        "actions": [
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            },
                            {
                                "action": "setHiddenAction",
                                "trigger": "now",
                                "target": "collectionView_spinner",
                                "data": {
                                    "hidden": true
                                }
                            }
                        ]
                    }
                ]
            }
        } );
    }

    SEP.debug = debug;
    SEP.timer = timer;
    SEP.getWebKeyValueStore = getWebKeyValueStore;

    SEP.login = SEP.login || {};
    SEP.login.captureEmail = captureEmail; // This is no longer used
    SEP.login.startService = startService;

    SEP.alert = SEP.alert || {};
    SEP.alert.logInWrongCredentials = logInWrongCredentials;
    SEP.alert.invalidCertificate = invalidCertificate;
    SEP.alert.hostConnectionError = hostConnectionError;
    SEP.alert.noEntitledPackages = noEntitledPackages;
    SEP.alert.forceUpdate = forceUpdate;
    SEP.alert.updateAvailable = updateAvailable;
    SEP.alert.deviceOffline = offlineAlert;

    SEP.contactSupport = contactSupport;
    SEP.sendFeedback = sendFeedback;
    SEP.loggedIn = false;

    window.sepStartupMessage = window.sepStartupMessage || {};
    window.sepStartupMessage.timeOfDay = timeOfDay;

    return SEP;

} ( SEP || {} ) );