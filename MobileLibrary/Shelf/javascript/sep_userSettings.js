/* global SM , SEP */

var userSettings = ( function() {
    this.getSetting = function( key ) {
        var propString = "userSettings." + key;
        return SM.getProperty( "#systemPackageDataRegistry" , propString );
    };

    this.setSetting = function( key , value ) {
        var action = {
            "action":"saveUserSettingAction",
            "target":"#systemPackageDataRegistry",
            "trigger":"now",
            "data":
            {
                "settingKey":key,
                "settingValue":value
            }
        };
        SM.runAction(action);
    };

    return this;
} () );



var sepEulaAgreement = {
    /*
        * @desc ScrollMotion Legal Terms
            If a bypass is necessary, replace instances of this function with sepEulaAgreement.bypassEULA
        * @param string - Legal Terms page id
        * @param string - Target page after accepting Legal Terms
        * @param string - optional Legal Terms version. If value exists it will be compared
            to a previous version and display terms if same value was not previously accepted
        * @return undefined
        * @required window.ISDEBUG - value must be true to print to console.
        * @required sepEulaAgreement.bypassEULA
    */
    showEulaOnce: function( firstUserPageTarget , acceptedPageTarget , eulaVersion ){
        sepEulaAgreement.showEulaOnce.eulaVersion = eulaVersion || "";
        SEP.debug( "Legal Terms: " + sepEulaAgreement.showEulaOnce.eulaVersion );

        var eulaWasDisplayedKey = "sep_eula_was_displayed",
            eulaWasDisplayed = userSettings.getSetting( eulaWasDisplayedKey ),
            eulaVersionAcceptedKey = "sep_eula_version_accepted",
            eulaVersionAccepted = userSettings.getSetting( eulaVersionAcceptedKey );
        SEP.debug("eulaWasDisplayed: " + eulaWasDisplayed + ", eulaVersionAccepted: " + eulaVersionAccepted );

        // Check if Legal Terms were accepted OR if the Legal Terms Version was accepted
        if( !eulaWasDisplayed || ( eulaVersion && eulaVersion !== eulaVersionAccepted ) ){
            userSettings.setSetting( eulaWasDisplayedKey , false );
            eulaWasDisplayed = userSettings.getSetting( eulaWasDisplayedKey );
            
            userSettings.setSetting( eulaVersionAcceptedKey , "" );
            eulaVersionAccepted = userSettings.getSetting( eulaVersionAcceptedKey );

            sepEulaAgreement.bypassEULA( false , firstUserPageTarget , acceptedPageTarget );
        } else {
            userSettings.setSetting( eulaWasDisplayedKey, true );
            sepEulaAgreement.bypassEULA( true , firstUserPageTarget , acceptedPageTarget );
        }
    },

    /*
        * @desc ScrollMotion Legal Terms bypass
        * @param boolean - Bypass terms to target page
        * @param string - Legal Terms page id
        * @param string - Target page after accepting Legal Terms
        * @return undefined
        * @required window.ISDEBUG - value must be true to print to console.
        * @required sepEulaAgreement.bypassEULA
        * @required SM
    */
    bypassEULA: function( bypassBool , firstUserPageTarget , acceptedPageTarget ) {
        // Application saveState doesn't behave. Return if taget page is current page
        if( SM.getActivePageId() === firstUserPageTarget || SM.getActivePageId() === acceptedPageTarget ) {
            return;
        }
        var targetPageId,
            targetPageTransition;

        if( bypassBool === true ) {
            targetPageId = acceptedPageTarget;
            targetPageTransition = "crossfade";
            SEP.login.startService( "downloadPackageMetaData" );
        }
        else {
            targetPageId = firstUserPageTarget;
            targetPageTransition = "slideup";
        }
        
        SM.runAction( {
            "action": "gotoPageAction",
            "trigger": "now",
            "target": "#systemPage",
            "data": {
                "pageId": targetPageId,
                "transitionDuration": 0.5,
                "transitionType": targetPageTransition
            }
        } );
    },

    acceptEULA: function( acceptedPageTarget , targetPageTransition ) {
        var eulaWasDisplayedKey = "sep_eula_was_displayed",
            eulaVersionAcceptedKey = "sep_eula_version_accepted";
        
        userSettings.setSetting( eulaWasDisplayedKey , true );
        userSettings.setSetting( eulaVersionAcceptedKey , sepEulaAgreement.showEulaOnce.eulaVersion );

        SEP.login.startService( "downloadPackageMetaData" );
        SEP.login.startService( "downloadPackageMetaData_login" );
        
        SM.runAction( {
            "action": "gotoPageAction",
            "trigger": "now",
            "target": "#systemPage",
            "data": {
                "pageId": acceptedPageTarget,
                "transitionDuration": 0.3,
                "transitionType": targetPageTransition
            }
        } );
    },

    upgradeIOS: function( upgradeNeeded , updatePageTarget ) {
        if( upgradeNeeded ) {
            SM.runAction( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage",
                "data": {
                    "pageId": updatePageTarget,
                    "transitionDuration": 0.3,
                    "transitionType": "crossfade"
                }
            } );
        }
        else {
            SM.runAction( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage",
                "data": {
                    "pageId": "page_login",
                    "transitionDuration": 0.5,
                    "transitionType": "crossfade"
                }
            } );
        }
    }
};