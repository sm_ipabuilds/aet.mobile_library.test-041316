// Custom javascript
// Created by Jon May X.X.X
/* global SM, SEP, userSettings */

// TO DO:
//  Consider a method to unregister a collectionView
//  Replace isIpad with device type callback function

( function( SEP ) {
    "use strict";
    
    var isIpad = ( SM.getScreenSize().width === 1024 || SM.getScreenSize().height === 1024 ) ? true : false;

    /***************************************************************************/
    /***************************************************************************/
    /****************** Borg actions convenience functions *********************/
    /***************************************************************************/
    /***************************************************************************/

    /*
        * @desc convenience function for set hidden action
        * @param array targets - target overlay ids
        * @param boolean hidden - are the targets to be hidden or visible
        * @return undefined
        * @required SM
    */
    function setHidden( targets , hidden ) {
        SM.runAction( {
            "action": "setHiddenAction",
            "trigger": "now",
            "targets": targets,
            "data": {
                "hidden": hidden
            }
        } );
    }

    // Ideally "setToggleButtonAction" would only be used but alternative actions
    // are required for grid view sort toggle buttons to behave properly
    function toggleButtonOn( overlayId , toggleOn ) {
        var action;
        if( arguments[ 2 ] && arguments[ 2 ].toLowerCase() === "usealternative" ) {
            action = {
                "action": ( toggleOn === true ) ? "toggleButtonOn" : "toggleButtonOff",
                "trigger": "now",
                "target": overlayId
            };
        } else {
            toggleOn = ( toggleOn === true ) ? "on" : "off";
            action = ( {
                "action": "setToggleButtonAction",
                "trigger": "now",
                "target": overlayId,
                "data": {
                    "state": toggleOn
                }
            } );
        }
        SM.runAction( action );
    }

    /*
        * @desc convenience function for set default sort action
        * @param string collectionViewId - collection view overlay id
        * @param string sortType - sort type
        * @param boolean isAscending - define if the sort should ascend or decend in order
        * @return undefined
    */
    function setDefaultSort( collectionViewId , sortType , isAscending ) {
        isAscending = (isAscending === false || isAscending === "NO") ? false : true;

        // The expected sort order for "lastModify" is inverted
        if( sortType === "lastModify" ) { isAscending = !isAscending; }

        var target = "#systemPackageDataRegistry",
            action = {
            "action": "setDefaultSort",
            "trigger": "now",
            "source": collectionViewId,
            "target": target,
            "data": {
                "overwrite": true,
                "sortUsing": sortType,
                "ascending": isAscending,
                "sortAll": "YES"
            }
        };
        if( sortType === "title" ) { action.data.caseInsensitive = true; }
        
        SM.runAction( action );
    }

    /*
        * @desc convenience function for sort items action
        * @param string collectionViewId - collection view overlay id
        * @param string sortType - sort type
        * @param boolean isAscending - define if the sort should ascend or decend in order
        * @return undefined
    */
    function setSortAction( collectionViewIds , sortType, isAscending ) {
        isAscending = (isAscending === false || isAscending === "NO") ? false : true;

        // The expected sort order for "lastModify" is inverted
        if( sortType === "lastModify" ) { isAscending = !isAscending; }

        var action = {
            "action": "sortItems",
            "trigger": "now",
            "data": {
                "overwrite": true,
                "sortUsing": sortType,
                "ascending": isAscending,
                "sortAll": "YES"
            }
        };
        if( sortType === "title" ) { action.data.caseInsensitive = true; }

        // This doesn't every appear to be anything but an array
        if( typeof collectionViewIds === "string" ) {
            action.source = action.target = collectionViewIds;
        } else {
            action.targets = collectionViewIds;
        }

        SM.runAction( action );
    }

    /*
        * @desc convenience function for load data action
        * @param string collectionViewId - collectionView overlay id to be targeted
        * @param string packageId - package id for default directory view
        * @return undefined
    */
    function loadDataAction( collectionViewIds , data ) {
        var action = {
            "action": "loadDataAction",
            "trigger": "now",
            "data": data
        };

        if( typeof collectionViewIds === "string" ) {
            action.source = action.target = collectionViewIds;
        } else {
            action.targets = collectionViewIds;
        }

        SM.runAction( action );
    }

    function loadDataActionSearch( collectionViewId , source , data ) {
        var action = {
            "action": "loadDataAction",
            "trigger": "now",
            "source": source,
            "target": collectionViewId,
            "data": data
        };

        SM.runAction( action );
    }

    /*
        * @desc load saved model.viewHistory to persistent memory
        * @return undefined
    */
    function loadPersistViewHistory() {
        try {
            var arrayString = userSettings.getSetting( "sep_view_history_array" );
            Model.viewHistory = JSON.parse( arrayString );
        } catch( e ) {
            Model.viewHistory = [];
        }
        SEP.debug( "Loaded view history: " , JSON.stringify( Model.viewHistory ) );
    }

    /*
        * @desc save model.viewHistory to persistent memory
        * @return undefined
    */
    function savePersistViewHistory() {
        var arrayString = JSON.stringify( Model.viewHistory );
        userSettings.setSetting( "sep_view_history_array" , arrayString );
    }

    /*
        * @desc load saved sort to persistent memory
        * @return undefined
    */
    function loadPersistSort() {
        try {
            var objString = userSettings.getSetting( "sep_sort_array" );
            Model.sortHistory = JSON.parse( objString );
        } catch( e ) {
            Model.sortHistory = [];
        }
        SEP.debug( "Loaded sort: " , JSON.stringify( Model.sortHistory ) );
    }

    /*
        * @desc save sort to persistent memory
        * @return undefined
    */
    function savePersistSort() {
        var arrayString = JSON.stringify( Model.sortHistory );
        userSettings.setSetting( "sep_sort_array" , arrayString );
    }

    /*
        * @desc load saved Model.packageHistory
        * @return undefined
    */
    function loadPersistHistory() {
        try {
            var arrayString = userSettings.getSetting( "sep_history_array" );
            Model.packageHistory =  JSON.parse( arrayString );
        } catch( error ) {
            //blank packageHistory
            Model.packageHistory = [];
        }
        SEP.debug( "Loaded history: " , Model.packageHistory );
    }
    
    /*
        * @desc save Model.packageHistory
        * @return undefined
    */
    function savePersistHistory() {
        var arrayString = JSON.stringify( Model.packageHistory );
        userSettings.setSetting( "sep_history_array" , arrayString );
    }

    function loadUserSettings() {
        SEP.debug("******* LOAD USER SETTINGS ********");
        loadPersistHistory();
        loadPersistViewHistory();
        loadPersistSort();
    }

    function executeAsync( func ) {
        var args = Array.prototype.slice.call( arguments ).slice( 1 );
        SM.setTimeout( function() {
            func.apply( null , args );
        } , 0 );
    }

    /***************************************************************************/
    /***************************************************************************/
    /*************** MobileLibrary Metadata and View Functions  ****************/
    /***************************************************************************/
    /***************************************************************************/

    function Model() {
        this.defaults = {
            view: {
                pageID: "Page_cloud_home",
                filter: "allFiles"
            },
            sort: {
                type: "title",
                ascending: true
            }
        };
        this.viewData = {
            header: {
                titleId: "collectionView_title",
                backButtonId: "collectionView_backBtn",
                backButtonTextId: "collectionView_backBtn_text",
                optionsButtonId: "options_gearBtn",
                optionsId_grid: "options_container",
                optionsId_list: "options_container_listView"
            },
            downloadedFiles: {
                optionsButtonId: "options_btn_localFiles",
                title: "Downloaded Files",
                returnNonHierarchicalData: true,
                filters: {
                    packageStates: [ "downloadQueued" , "downloadQueuedwifi" , "downloadStarted" , "downloadCompleted" , "installReady" , "installQueued" , "installStarted", "installCompleted" , "statePolicyWaitUser" , "downloadAvailable" , "invalid" ]
                }
            },
            downloadQueue: {
                optionsButtonId: "options_btn_downloadQueue",
                title: "Download Queue",
                returnNonHierarchicalData: true,
                filters: {
                    packageStates: [ "downloadQueued" , "downloadQueuedwifi" , "downloadStarted" , "downloadCompleted" , "installReady" , "installQueued" , "installStarted" ]
                }
            },
            allFiles: {
                optionsButtonId: "options_btn_allFiles",
                title: "MobileLibrary",
                returnNonHierarchicalData: false,
                filters: {}
            }
        };
        this.sortData = {
            list: {
                title:{
                    buttonId:"sortType_btn_listView_name",
                    imageId:"sep_sort_btn_arrow_name"
                },
                type: {
                    buttonId:"sortType_btn_listView_kind",
                    imageId:"sep_sort_btn_arrow_kind"
                },
                lastModify:{
                    buttonId:"sortType_btn_listView_date",
                    imageId:"sep_sort_btn_arrow_date"
                },
                size:{
                    buttonId:"sortType_btn_listView_size",
                    imageId:"sep_sort_btn_arrow_size"
                }
            },
            grid: {
                title:{
                    buttonId:"sortType_btn_name_myFiles"
                },
                lastModify:{
                    buttonId:"sortType_btn_date_myFiles"
                }
            }
        };
        return this;
    }
    Model.collectionViewIds = [];
    Model.viewHistory = [];
    Model.sortHistory = [];
    Model.packageHistory = [];
    Model.searchMode = false;

    var View = {
        loadData: function( event , data ) {
            var collectionViewIds = data.collectionViewIds;
            if( data.returnNonHierarchicalData ) {
                data.defaultCollection = "";
            }
            try {
                loadDataAction( collectionViewIds , data );
            } catch( e ) {
                SEP.debug( true , "loadDataActionFailure: " + e );
            }
        },
        updateView: function( viewType , data ) {
            var header = data.header,
                showBackButton = ( data.isRoot || data.returnNonHierarchicalData ) ? true : false ;
            SM.setText( header.titleId , SEP.updateTranslationForString( data.title ) );
            setHidden( [ header.titleId , header.optionsButtonId ] , false );

            // Data is nested in folders
            if( !data.returnNonHierarchicalData ) {
                callbackOnHistoryLength( "root" , function() {
                    setHidden( [ header.backButtonId ] , true );
                } );
                callbackOnHistoryLength( "notroot" , function() {                
                    setHidden( [ header.backButtonId ] , showBackButton );
                } );                
            } else {
                setHidden( [ header.backButtonId ] , true );            
            }  
            if( viewType === "downloadQueue" ) {
                setHidden( [ header.optionsId_grid , header.optionsId_list ] , true );
            } else {
                setHidden( [ header.optionsId_grid , header.optionsId_list ] , false );
                SEP.device.callbackOnDevice( "ipad" , function() {
                    SM.setText( header.backButtonTextId , data.parentDirectoryTitle );
                } );
            }
        },

        /*
            * @desc load collection view data, back button title and states, and title text
            * overlayId "collectionView_backBtn_text" is **** HARD-CODED ****
            * @param string backButtonId - back button overlay id to target for text and hidden state
            * @param string collectionViewId - collectionView overlay id 
            * @param string titleId - overlay id to apply title text
            * @return undefined
        */
        backButtonAction: function() {
            var packageId = getParentPackageId(),
                header = new Model().viewData.header;
            // packageId should exist but an empty String data type is acceptable
            if( packageId || ( typeof packageId === "string" && packageId.length === 0 ) ) {
                removeHistory( packageId );
                // Load collection view data
                loadDataAction( Model.collectionViewIds , { defaultCollection: packageId } );

                // Set back button text
                SEP.device.callbackOnDevice( "ipad" , function() {
                    SM.setText( header.backButtonTextId , getPackageTitle( "parent" ) );
                } );
                SEP.device.callbackOnDevice( "iphone" , function() {
                    SM.setText( header.backButtonTextId , SEP.updateTranslationForString( "Back" ) );
                } );
                SM.setText( header.titleId , getPackageTitle( "current" ) );
                
                callbackOnHistoryLength( "root" , function() {
                    setHidden( [ header.backButtonId ] , true );
                } );
            }
        },

        loadSort: function( event , data ) {
            if( !data.disableToggle ) {
                // Apply sorting to all registered CollectionViews
                setSortAction( Model.collectionViewIds , event , data.targetSortAscending );
            }
            // CollectionView id is not necessary
            setDefaultSort( Model.collectionViewIds[ 0 ] , event , data.targetSortAscending );
        },
        optionsSortToggle: function( event , data ) {
            var space = "        ",
                grid = data.grid[ event ];

            if( data.targetSortAscending === true ) {
                if( event === "title" ) {
                    SM.setText( grid.buttonId , space + SEP.updateTranslationForString( "A - Z" ) );
                } else if( event === "lastModify" ) {
                    SM.setText( grid.buttonId , space + SEP.updateTranslationForString( "Newest - Oldest" ) );
                }
            } else {
                if( event === "title" ) {
                    SM.setText( grid.buttonId , space + SEP.updateTranslationForString( "Z - A" ) );
                } else if( event === "lastModify" ) {
                    SM.setText( grid.buttonId , space + SEP.updateTranslationForString( "Oldest - Newest" ) );
                }
            }
            if( data.previousSortType !== event ) {
                if( data.previousSortType === "title" ) {
                    SM.setText( data.grid[ data.previousSortType ].buttonId , space + SEP.updateTranslationForString( "A - Z" ) );
                } else if( data.previousSortType === "lastModify" ) {
                    SM.setText( data.grid[ data.previousSortType ].buttonId , space + SEP.updateTranslationForString( "Newest - Oldest" ) );
                }
            }

            if( event === "lastModify" ) {
                SM.moveOverlay( "sep_settings_checkmark_sort" , "30px" , "112px" );
                setHidden( [ "sep_settings_checkmark_sort" ] , false );
                toggleButtonOn( grid.buttonId , true , "useAlternative" );

            } else if( event === "title" ) {
                SM.moveOverlay( "sep_settings_checkmark_sort" , "30px" , "72px" );
                setHidden( [ "sep_settings_checkmark_sort" ] , false , "useAlternative" );
                toggleButtonOn( grid.buttonId , true );
            } else {
                setHidden( [ "sep_settings_checkmark_sort" ] , true );
            }
        },
        listSortToggleImage: function ( event , data ) {
            var image = "",
                text,
                list = data.list[ event ];
            
            if( data.targetSortAscending === true ) {
                image = "art_assets/iPad/sort_arrow_up.png";
                text = "Up_arrow";
            } else {
                image = "art_assets/iPad/sort_arrow_down.png";
                text = "Down_arrow";
            }
            if( data.previousSortType !== event ) {
                setHidden( [ data.list[ data.previousSortType ].imageId ] , true );
            }
            setHidden( [ list.imageId ] , false );

            // SM.runAction( {
            //     "action": "setImageAction",
            //     "trigger": "now",
            //     "target": list.imageId,
            //     "data": {
            //         "image": image
            //     }
            // } );

            SM.runAction( {
                "action": "setTextAction",
                "trigger": "now",
                "target": list.imageId,
                "data": {
                    "text": text
                }
            } );
        },

        /*
            * @desc options panel actions to set sort, view, and show to current view
            * @return undefined -
            * @required view.optionsSortToggle function
        */
        optionsPanelDisplay: function() {
            var sort = getSortCache(),
                filter = SEP.getPreviousView(),
                viewType = filter.view,
                sortData = new Model().sortData;

            var data = new Model().viewData,
                header = data.header;
            data = data[ viewType ];
            data.header = header;

            SEP.debug(viewType);

            sortData.targetSortAscending = sort.ascending;
            View.optionsSortToggle( sort.type , sortData );

            if( "Page_cloud_home" === SM.getActivePageId() ) {
                SM.moveOverlay( "sep_settings_checkmark_view" , "30px" , "72px" );
                toggleButtonOn( "options_btn_gridView" , true );
            } else if( "Page_cloud_home_listView" === SM.getActivePageId() ) {
                SM.moveOverlay( "sep_settings_checkmark_view" , "30px" , "112px" );
                toggleButtonOn( "options_btn_listView" , true );
            }

            if( viewType === "downloadedFiles" ) {
                SM.moveOverlay( "sep_settings_checkmark_show" , "30px" , "112px" );
            } else if( viewType === "allFiles" ) {
                SM.moveOverlay( "sep_settings_checkmark_show" , "30px" , "72px" );
            }
            toggleButtonOn( data.optionsButtonId , true );
        },

        /*
            * @desc load packages meeting search criteria with view filters
            * @param object collectionViewObj - registered collection view object
            * @return undefined - 
        */
        searchByPackageTitle: function( collectionViewObj , data ) {
            // Provide filterEmptyActionId
            var filter = getViewHistory( "current" ).filter;
            filter = data[ filter ].filters.packageStates;
            this.searchTimeout = SM.setTimeout(function(){
                loadDataActionSearch( collectionViewObj.getCollectionViewId() , Model.searchTextfield , {
                    returnNonHierarchicalData: true,
                        filters: {
                            packageStates: filter,
                            nameOptions: {
                                searchString: Model.searchString
                            }
                        },
                        events: {
                            filterEmptyActionId: "sepFilterResultEmptyAction_search"
                        }
                } );
            }, 500);
        },
        searchTimeout: null,
        cancelSearch: function(){
            if(this.searchTimeout){
                SM.clearTimeout(this.searchTimeout);
                this.searchTimeout = null;
            }
        },
        spawnPassiveMessage: function( pluginId , data ) {
            var page = getViewHistory( "current" ),
                view = page.filter;
            page = page.pageID;

            if( data[ page ] && data[ page ].fontColor ) {
                data.fontColor = data[ page ].fontColor;
            }
            if( data[ view ] && data[ view ].text ) {
                data.text = data[ view ].text;
            }
            SM.runAction( {
                "action": "displayPassiveMessageAction",
                "trigger": "now",
                "target": pluginId,
                "data": data
            } );
        },
        dismissPassiveMessage: function( pluginId ) {
            SM.runAction( {
                "action": "dismissPassiveMessageAction",
                "trigger": "now",
                "target": pluginId,
                "data": {}
            } );
        }
    };

    /***************************************************************************/
    /***************************************************************************/
    /********************** MobileLibrary Shelf View ***************************/
    /***************************************************************************/
    /***************************************************************************/

    /*
        * @desc trigger view events associated with registered collection view objects
        * Additionally, modifies publish function args with view data
        * @param string topic - see publish function
        * @param object args - see publish function
        * @return string - see publish function
    */
    var triggerViewEvent = SEP.events.publish( function( event , args ) {
        addViewHistory( event );
        // Views can be cached and updated asynchronously
        executeAsync( View.updateView , event , args );
    } );

    /*
        * @desc Add view filter to model.viewHistory array for back reference
        * @param string filter - view filter can support "allFiles", "downloadQueue", and "downloadedFiles"
        * @return undefined
    */
    function addViewHistory( filter ) {
        var pageId = SM.getActivePageId(),
            len = Model.viewHistory.length;

        if( len > 0 ) {
            // Do not add sequential duplicate views
            if( Model.viewHistory[ len - 1 ].pageID === pageId &&
                Model.viewHistory[ len - 1 ].filter === filter ) {
                return;
            }
        }
        // Don't store more than 10 sorts to Model.viewHistory
        if( len >= 10 ) { Model.viewHistory = Model.viewHistory.slice( - 10 ); }

        Model.viewHistory.push( {
            pageID: pageId,
            filter: filter
        } );

        executeAsync( savePersistViewHistory );
        SEP.debug( JSON.stringify( Model.viewHistory ) );
    }
    
    /*
        * @desc get view filter and page id cached in history
        * @param string name - "current" or "previous"
        * @return object - return "current" or "previous" view filter with page id
    */
    function getViewHistory( name ) {
        var viewHistory = Model.viewHistory,
            i = viewHistory.length - 1,
            defaultView = new Model().defaults.view;
        if( viewHistory.length === 0 ) { return defaultView; }
        name = name.toLowerCase();
        if( name === "current" ) {
            return viewHistory[ i ];
        } else if( name === "previous" ) {
            return ( i === 0 ) ? defaultView : viewHistory[ i - 1 ];
        }
    }

    function setView( viewType ) {
        var len = Model.packageHistory.length,
            data = new Model().viewData,
            header = data.header;
        data = data[ viewType ];
        data.header = header;

        if( !data.returnNonHierarchicalData ) {
            if( len === 0 ) {
                addHistory( "" , data.title );
                data.isRoot = true;
            }
            data.title = getPackageTitle( "current" );
            data.parentDirectoryTitle = getPackageTitle( "parent" );
        }
        data.collectionViewIds = Model.collectionViewIds;
        data.defaultCollection = getPackageId( "current" );

        SEP.dismissPassiveMessage( "alert_plugin" );

        triggerViewEvent( viewType , data );
    }

    /***************************************************************************/
    /***************************************************************************/
    /********************** MobileLibrary Shelf Sort ***************************/
    /***************************************************************************/
    /***************************************************************************/

    /*
        * @desc return sort cache object
        * @return object - object containing sort type and sort ascending values
    */
    function getSortCache( name ) {
        var sortHistory = Model.sortHistory,
            len = sortHistory.length;

        if( len === 0 ) {
            return new Model().defaults.sort;
        } else if( name && name.toLowerCase() === "previous" && len >= 2 ) {
            return sortHistory[ len - 2 ];
        } else {
            return sortHistory[ len - 1 ];
        }
    }

    /*
        * @desc convenience function to set sort type and ascending values in cache
        * @return undefined - 
    */
    function setSortCache( sortType, isAscending ) {
        var len = Model.sortHistory.length;
        // Don't store more than 2 sorts to Model.sortHistory
        if( len >= 2 ) { Model.sortHistory = Model.sortHistory.slice( 1 ); }
        Model.sortHistory.push( { type: sortType , ascending: isAscending } );
        executeAsync( savePersistSort );
    }

    function setSort( sortType ) {
        var data = new Model().sortData,
            sort = getSortCache(),
            isAscending = sort.ascending;
        sort = sort.type;
        data.disableToggle = false;

        if( arguments[ 1 ] && arguments[ 1 ].disableToggle ) {
            data.disableToggle = true;
        } else {
            isAscending = ( sortType === sort ) ? !isAscending : true;
        }

        // Inject data with current sort info
        data.previousSortType = sort;
        data.targetSortAscending = isAscending;

        setSortCache( sortType , isAscending );
        SEP.events.publish( sortType , data );
    }

    /***************************************************************************/
    /***************************************************************************/
    /************************** MobileLibrary RegisterCV ***********************/
    /***************************************************************************/
    /***************************************************************************/

    function RegisterCV( collectionViewId, data ) {
        RegisterCV.subscribed = RegisterCV.subscribed || false;
        RegisterCV.tokens = RegisterCV.tokens || [];

        // Cache unique registered model.collectionViewIds
        if( Model.collectionViewIds.indexOf( collectionViewId ) === -1 ) {
            Model.collectionViewIds.push( collectionViewId );
        }

        SM.log( "RegisterCollectionView" );

        // Ensure subscriptions are not duplicated
        if( RegisterCV.subscribed === false ) {
            loadUserSettings();
            // loadPersistHistory();
            // loadPersistViewHistory();
            // loadPersistSort();

            RegisterCV.tokens.push( SEP.events.subscribe( "allFiles" , View.loadData ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "downloadedFiles" , View.loadData ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "downloadQueue" , View.loadData ) );
                        
            RegisterCV.tokens.push( SEP.events.subscribe( "title" , View.loadSort ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "type" , View.loadSort ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "lastModify" , View.loadSort ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "size" , View.loadSort ) );

            RegisterCV.tokens.push( SEP.events.subscribe( "title" , View.optionsSortToggle ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "lastModify" , View.optionsSortToggle ) );

            RegisterCV.tokens.push( SEP.events.subscribe( "title" , View.listSortToggleImage ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "type" , View.listSortToggleImage ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "lastModify" , View.listSortToggleImage ) );
            RegisterCV.tokens.push( SEP.events.subscribe( "size" , View.listSortToggleImage ) );

            RegisterCV.subscribed = true;
        }

        this.getDirectoryTitle = getPackageTitle;
        this.getDirectoryId = getPackageId;
        this.clearShelfHistory = clearShelfHistory;
        this.getCollectionViewId = function() { return collectionViewId; };

        this.loadDataAction = function() {
            var viewType = getViewHistory( "current" ).filter;

            var data = new Model().viewData,
                header = data.header;
            data = data[ viewType ];
            data.header = header;

            data.collectionViewIds = Model.collectionViewIds;
            data.defaultCollection = getPackageId( "current" );

            View.loadData( viewType , data );
            return this;
        };

        /*
            * @desc load view information associated with collection view objects
            * @return object - returns this Object allowing function chaining
        */
        this.loadViewAction = function() {
            var viewType = getViewHistory( "current" ).filter;
            viewType = ( viewType !== "downloadQueue" ) ? viewType :
                getViewHistory( "previous" ).filter || data.viewType;

            setView( viewType );
            return this;
        };
        this.setView = setView;

        this.loadSortAction = function() {
            setSort( getSortCache().type , { disableToggle: true } );
            return this;
        };
        this.setSort = setSort;

        this.clear = function() {
            var len = Model.collectionViewIds.length;
            if( len && len <= 1 ) {
                RegisterCV.subscribed = false;
                clearShelfHistory();
                clearViewHistory();
                clearSortHistory();

                for( var i = 0 , tokenLen = RegisterCV.tokens.length ; i < tokenLen ; i++ ) {
                    SEP.events.unsubscribe( RegisterCV.tokens[ i ] );
                }
            }
            Model.collectionViewIds.splice( Model.collectionViewIds.indexOf( collectionViewId ) , 1 );
        };

        return this;
    }

    /***************************************************************************/
    /***************************************************************************/
    /********************** MobileLibrary Shelf Breadcrumbs ********************/
    /***************************************************************************/
    /***************************************************************************/

    RegisterCV.prototype.getCollectionViewIds = function() { return Model.collectionViewIds; };
    
    /*
        * @desc add new directory to array and set collectionView to directory, 
        * additionally, set title and back button with the appropriate state and text
        * overlayId "collectionView_backBtn_text" is **** HARD-CODED ****
        * @param string packageId - package id of directory
        * @param string packageTitle - package title for display
        * @return undefined
    */
    RegisterCV.prototype.addNewDirectory = function() {
        var header = new Model().viewData.header;
        addHistory.apply( this , Array.prototype.slice.call( arguments ) );
        loadDataAction( this.getCollectionViewId() , { defaultCollection: arguments[ 0 ] } );
        
        SM.setText( header.titleId , getPackageTitle( "current" ) );
        
        SEP.device.callbackOnDevice( "ipad" , function() {
            SM.setText( header.backButtonTextId , getPackageTitle( "parent" ) );
        } );
        SEP.device.callbackOnDevice( "iphone" , function() {
            SM.setText( header.backButtonTextId , SEP.updateTranslationForString( "Back" ) );
        } );
        
        callbackOnHistoryLength( "notRoot" , function() {
            setHidden( [ header.backButtonId ] , false );
        } );
        
        savePersistHistory();
    };

    /*
        * @desc back button actions upon tapping back button
        * @param string - optionally apply to all views with "applytoallviews"
        * @return undefined
        * @required backButtonAction
    */
    RegisterCV.prototype.backButtonAction = function() {
        var header = new Model().viewData.header;
        SEP.dismissPassiveMessage( "alert_plugin" );
        
        View.backButtonAction( header.backButtonId , Model.collectionViewIds , header.titleId );
        
        savePersistHistory();
    };

    /*
        * @desc Actions for collection view refresh
        * @return undefined -
    */
    RegisterCV.prototype.refreshAction = function() {
        var CVObject = this;
        SEP.dismissPassiveMessage( "alert_plugin" );
        
        SM.runAction({
            "action": "checkPackagesWaitingUserInstall",
            "trigger": "now",
            "target": "#systemPackageDataRegistry"
        });

        SEP.network.callbackOnConnection( "online" , function() {

            if( isIpad === true ) {
                if(Model.searchMode) {
                    CVObject.focusSearchWithString();
                }
                else {
                    CVObject.loadDataAction();                    
                }
            } else {
                loadDataAction( CVObject.getCollectionViewId() , { defaultCollection: CVObject.getDirectoryId( "current" ) } );
            }
            
            SM.runAction( {
                "action": "verifyAllInstalledPackagesAction",
                "trigger": "now",
                "target": "#systemPackageDataRegistry",
                "source": "#keyValueStore",
                "data": {
                    "message":"#sepPackageMissing"
                }
            } );
        } );

        SEP.network.callbackOnConnection( "offline" , function() {
            var collectionIds = [];
            if( isIpad === true ) {
                collectionIds = CVObject.getCollectionViewIds();
            } else {
                collectionIds.push( CVObject.getCollectionViewId() );
            }
            SM.runAction( {
                "action": "endPullRefreshAction",
                "trigger": "now",
                "targets": collectionIds
            });
            SEP.network.deviceOffLineAction();
        } );
    };

    /*
        * @desc add directory data to Model.packageHistory
        * @param string packageId - package id of directory
        * @param string packageTitle - package title for display
        * @return undefined
    */
    function addHistory( packageId , packageTitle ) {
        var packageHistory = Model.packageHistory,
            len = packageHistory.length;
        if( len > 0 ) {
            // Do not add sequential duplicate packages
            if( packageHistory[ len - 1 ].packageID === packageId ) {
                return;
            }
            // Set directory as parent directory
            packageHistory[ len - 1 ].isParentDir = true;
        }
        packageHistory.push( newHistory( packageId , packageTitle ) );
    }

    function newHistory( packageId , packageTitle ) {
        return {
            packageID: packageId,
            title: packageTitle,
            isParentDir: false
        };
    }
    
    /*
        * @desc remove directory data from Model.packageHistory
        * @param string packageId - package id of directory
        * @return undefined
    */
    function removeHistory( packageId ) {
        var packageHistory = Model.packageHistory,
            len = packageHistory.length,
            i = len - 1,
            offset = -1;
        // Do not remove first package
        if( len === 1 ) { return; }

        for( i ; i >= 0 ; i-- ) {
            offset += 1;
            if( packageHistory[ i ].packageID === packageId ) {
                packageHistory.splice( -1 * offset , offset );
                packageHistory[ packageHistory.length - 1 ].isParentDir = false;
            }
        }
    }

    /*
        * @desc clear entire Model.packageHistory array
        * @return undefined
    */
    function clearShelfHistory() {
        Model.packageHistory = [];
        savePersistHistory();
    }

    /*
        * @desc clear entire Model.viewHistory array
        * @return undefined
    */
    function clearViewHistory() {
        Model.viewHistory = [];
        savePersistViewHistory();
    }

    /*
        * @desc clear entire Model.sortHistory array
        * @return undefined
    */
    function clearSortHistory() {
        Model.sortHistory = [];
        savePersistSort();
    }

    /*
        * @desc convenience function to get parent directory
        * @return number - value of first parent directory from end of Model.packageHistory
        * else, return negative number
    */
    function indexOfParentDirectory() {
        var packageHistory = Model.packageHistory,
            len = packageHistory.length,
            i = len - 1;

        for( i ; i >= 0 ; i-- ) {
            if( packageHistory[ i ].isParentDir === true ) {
                return i;
            }
        }
        return -1;
    }

    /*
        * @desc get data stored in Model.packageHistory
        * @param string name - identify "current" or "parent" directory data
        * @return object - object containing Model.packageHistory data
    */
    function getDirectoryData( name ) {
        var packageHistory = Model.packageHistory,
            i;
        name = name.toLowerCase();
        if( packageHistory.length > 0 ) {
            i = indexOfParentDirectory();
            if( name === "current" ) {
                return packageHistory[ i + 1 ];
            } else if( name === "parent" ) {
                if( i < 0 ) { i = 0; }
                return packageHistory[ i ];
            }
        } else {
            return newHistory( "" , "" );
        }
    }

    /*
        * @desc convenience function with localization 
        * @param string name - identify "current" or "parent" directory data
        * @return string - Model.packageHistory title
        * @required getDirectoryData
    */
    function getPackageTitle( name ) {
        if( SEP.updateTranslationForString ) {
            return SEP.updateTranslationForString( getDirectoryData( name ).title );
        } else {
            return getDirectoryData( name ).title;
        }
    }

    /*
        * @desc convenience function 
        * @param string name - identify "current" or "parent" directory data
        * @return string - Model.packageHistory packageId
        * @required getDirectoryData
    */
    function getPackageId( name ) {
        return getDirectoryData( name ).packageID;
    }

    /*
        * @desc convenience function 
        * @return string - parent Model.packageHistory packageId
        * @required getDirectoryData
    */
    function getParentPackageId() {
        return getDirectoryData( "parent" ).packageID;
    }

    /*
        * @desc execute function on Model.packageHistory length, or, for convenience,
        * execute function when Model.packageHistory is or is not at root level
        * @param string or number type - string identifying Model.packageHistory length is "root" or "notRoot"
        * or, number identifying Model.packageHistory length
        * @param function callback - function to be executed
        * @return undefined
    */
    function callbackOnHistoryLength( type , callback ) {
        var packageHistory = Model.packageHistory;
        if( typeof type === "number" && type % 1 === 0 ) {
            if( type > 0 && type === packageHistory.length ) {
                callback();
            }
        } else if( typeof type === "string" ) {
            type = type.toLowerCase();
            if( type === "root" && packageHistory.length === 1 ) {
                callback();
            } else if( type === "notroot" && packageHistory.length > 1 ) {
                callback();
            }
        }
    }

    /***************************************************************************/
    /***************************************************************************/
    /************************* MobileLibrary Shelf Search **********************/
    /***************************************************************************/
    /***************************************************************************/

    /*
        * @desc textfield behavior when user begins search input
        * Including clear button visibility and partial search results
        * @param string textfield - textfield overlay id
        * @param string textString - textfield string output from textfield, '#(text)'
        * @return undefined - 
        * @required scrollMotionDownloadViewObjects
    */
    RegisterCV.prototype.focusSearchWithString = function( textfield , textString ) {
        View.cancelSearch();
        if(textfield && textString){
            Model.searchTextfield = textfield;
            Model.searchString = textString;
            setHidden( [ "search_btn_textfield_clear" , "search_btn_textfield_clear_listView" ] , false );          
            View.searchByPackageTitle( this , new Model().viewData );
        } else {
            setHidden( [ "search_btn_textfield_clear" , "search_btn_textfield_clear_listView" ] , true );
            this.loadDataAction();
        }
        Model.searchMode = true;          
        View.dismissPassiveMessage( "alert_plugin" );
    };

    /*
        * @desc clear button functionality reseting search string, hiding clear button
        * Additionally, restoring collection view packages for view filter
        * @return undefined - 
    */
    RegisterCV.prototype.clearSearchString = function() {
        View.cancelSearch();    
        Model.searchString = "";
        SM.setText( Model.searchTextfield , Model.searchString );
        setHidden( [ "search_btn_textfield_clear" , "search_btn_textfield_clear_listView" ] , true );
        View.dismissPassiveMessage( "alert_plugin" );
        Model.searchMode = false;
        this.loadDataAction();
    };

    /*
        * @desc clear button functionality reseting search string, hiding clear button
        * Additionally, restoring collection view packages for view filter, closing search functionality
        * @return undefined - 
    */
    RegisterCV.prototype.closeSearch = function() {
        View.cancelSearch();    
        Model.searchString = "";
        SM.setText( Model.searchTextfield , Model.searchString );
        setHidden( [ "search_btn_textfield_clear" , "search_btn_textfield_clear_listView" ] , true );
        Model.searchMode = false;
        View.dismissPassiveMessage( "alert_plugin" );
        this.loadViewAction();
    };



    /***************************************************************************/
    /***************************************************************************/
    /****************** Make private public, show your stuff ;) ****************/
    /***************************************************************************/
    /***************************************************************************/

    SEP.RegisterCollectionView = RegisterCV;
    SEP.optionsPanelDisplay = View.optionsPanelDisplay;
    SEP.spawnPassiveMessage = View.spawnPassiveMessage;
    SEP.dismissPassiveMessage = View.dismissPassiveMessage;
    SEP.loadUserSettings = loadUserSettings;

    SEP.userLogin = function( URL , serviceType ) {
        SEP.network.callbackOnConnection( "online" , function() {
            SM.runAction( {
                "action": "userLoginAction",
                "trigger": "now",
                "target": "#systemAuthentication",
                "data": {
                    "loginURL": URL,
                    "logoutURL": URL,
                    "serviceType": serviceType
                }
            } );
        } );
        SEP.network.callbackOnConnection( "offline" , function() {
            SEP.alert.deviceOffline();
        } );
    };

    /*
        * @desc user log out function with support for online/offline messaging
        * Additionally, provides timeout to ensure action is not called upon repeat tapping
        * To ensure spinner is not visible upon returning to login page
        * @return undefined -
    */
    SEP.userLogout = function() {
        SEP.userLogout.timeout = SEP.userLogout.timeout || undefined;
        var timeout = SEP.userLogout.timeout;

        SEP.network.callbackOnConnection( "offline" , function() {
            SM.runAction( {
                "action": "displayAlertAction",
                "target": "alert_plugin",
                "trigger": "now",
                "data": {
                    "title": SEP.updateTranslationForString( "You are not connected to the internet." ),
                    "body": SEP.updateTranslationForString( "Are you sure you want to log out? You won't be able to log in again until you connect to the internet." ),
                    "image": "art_assets/noWiFi.png",
                    "buttons": [
                        {
                            "text": SEP.updateTranslationForString( "Cancel" ),
                            "actions": [
                                {
                                    "action": "animate",
                                    "trigger": "now",
                                    "target": "options_gearBtn_optionsModal_bkgd",
                                    "data": {
                                        "animationId": "anim_fadeOut"
                                    }
                                },
                                {
                                    "action": "close",
                                    "trigger": "now",
                                    "delay": 0.25,
                                    "targets": [
                                        "options_gearBtn_optionsModal_bkgd",
                                        "options_gearBtn_optionsModal_details"
                                    ]
                                },
                                {
                                    "action": "dismissAlertAction",
                                    "trigger": "touchUpInside",
                                    "target": "alert_plugin"
                                }
                            ]
                        },
                        {
                            "text": SEP.updateTranslationForString( "Log out" ),
                            "actions": [
                                {
                                    "action": "setHiddenAction",
                                    "trigger": "touchUpInside",
                                    "target": "collectionView_spinner",
                                    "data":
                                    {
                                        "hidden": false
                                    }
                                },
                                {
                                    "action": "runScriptAction",
                                    "trigger": "touchUpInside",
                                    "target": "#jsBridge",
                                    "data": {
                                        "script": "SEP.dismissPassiveMessage( 'alert_plugin' );"
                                    }
                                },
                                {
                                    "action": "dismissAlertAction",
                                    "trigger": "touchUpInside",
                                    "target": "alert_plugin"
                                },
                                {
                                    "action": "userLogout",
                                    "trigger": "touchUpInside",
                                    "target": "#systemAuthentication"
                                }                                
                            ]
                        }
                    ]
                }
            } );
        } );
        SEP.network.callbackOnConnection( "online" , function() {
            if( typeof timeout === "undefined" || new Date().getTime() - timeout > 10000 ) {
                SEP.userLogout.timeout = new Date().getTime();
                clearShelfHistory();
                clearViewHistory();
                clearSortHistory();
                
                SM.runAction( {
                        "action": "userLogout",
                        "trigger": "now",
                        "target": "#systemAuthentication"
                    } );
                SM.runAction( {
                        "action": "setHiddenAction",
                        "trigger": "now",
                        "target": "collectionView_spinner",
                        "data":
                        {
                            "hidden":false
                        }
                    } );
                SEP.dismissPassiveMessage( "alert_plugin" );
            }
        } );
    };

    /*
        * @desc return view filter and page id of last view unless download queue
        * @return object - view filter and page id
    */
    SEP.getPreviousView = function() {
        var view = getViewHistory( "current" ),
            page = view.pageID;
        if( view.filter === "downloadQueue" ) {
            view = getViewHistory( "previous" );
            page = view.pageID;
        }
        return {
         pageId: page,
         view: view.filter
        };
    };
    SEP.updateTranslationForString = function( string ) {
        return SM.getProperty( "jsLocalizer" , JSON.stringify( { "op": "localizedString" , "string": string } ) );
    };

} ( SEP || {} ) );