// Custom Javascript
// Created by Jon May X.X.X

/* global SM, SEP */

( function( SEP ) {
    "use strict";
    var connectivity = false,
        connectionType = "";

    /*
        * @desc Set connectivity variable to true or false
        * @param boolean isConnected - 
        * @return undefined -
    */
    function setConnectivity( isConnected ) {
        if( typeof isConnected !== "boolean" ) { return; }
        connectivity = isConnected;
    }

    /*
        * @desc set connection type to either wifi or wwan
        * @param string type - "wifi" or "wwan"
        * @return undefined - 
    */
    function setConnectionType( type ) {
        type = type.toLowerCase();
        if( connectivity && type === "wifi" || type === "wwan" ) {
            connectionType = type;
        }
    }

    /*
        * @desc Execute function callback on valid connection type
        * @param string type - type can be "online", "offline", "wifi", "wwan"
        * @param function callback - if type is valid, execute callback function
        * @return undefined - 
    */
    function callbackOnConnection( type , callback ) {
        var connection = ( connectivity ) ? "online" : "offline";
        type = type.toLowerCase();
        if( connectivity ) {
            if( ( connection === "online" && type === connection ) || connectionType === type ) {
                callback();
            }
        } else if( connection === "offline" && type === connection ) {
            callback();
        }
    }

    function callbackOnDevice( type , callback ) {
        callbackOnDevice.width = callbackOnDevice.width || 0;
        callbackOnDevice.height = callbackOnDevice.height || 0;
        var screenSize,
            orientation = SM.getOrientation(),
            isDevice = false,
            isDeviceType = function( type ) {
                var regex = this.match( new RegExp( type , "g" ) );

                if( regex && regex.toString().toLowerCase() === type ) {
                    return true;
                }
                return false;
            },
            hasNumericalSuffix = function( suffix ) {
                var args =  Array.prototype.slice.call( arguments ).slice( 1 ),
                    hasSuffix = false,
                    key;
                if( !suffix ) { return true; } // Return true if no suffix
                for( key in args ) {
                    if( suffix[ 0 ] === args[ key ] ) {
                        hasSuffix = true;
                    }
                }
                return hasSuffix;
            },
            hadDeviceSize = function( size ) {
                return ( size === callbackOnDevice.width || size === callbackOnDevice.height ) ? true : false;
            };

        if( !callbackOnDevice.width && !callbackOnDevice.height ) {
            screenSize = SM.getScreenSize();

            // iOS 8 SDK changes the behavior of SM.getScreenSize
            // callbackOnDevice.width = screenSize.width;
            // callbackOnDevice.height = screenSize.height;

            // Normalize SM.getScreenSize
            /* Begin workaround to new behavior */
            if( orientation === "landscape" ) {
                // If iOS 8 SDK, width will be greater than height, else return size
                if( screenSize.width > screenSize.height ) { 
                    callbackOnDevice.width = screenSize.height;
                    callbackOnDevice.height = screenSize.width;
                } else {
                    callbackOnDevice.width = screenSize.width;
                    callbackOnDevice.height = screenSize.height;
                }
            } else {
                callbackOnDevice.width = screenSize.width;
                callbackOnDevice.height = screenSize.height;
            }
            /* End workaround */
        }

        if( type.toLowerCase() === "any" ) {
            isDevice = true;
        } else if( isDeviceType.call( type , "ipad" ) && hadDeviceSize( 1024 ) ) {
            isDevice = true;
        } else if( isDeviceType.call( type , "iphone" ) ) {
            if( hadDeviceSize( 568 ) ) {
                isDevice = hasNumericalSuffix( type.match( /\d+$/g ) , "5" );
            } else if( hadDeviceSize( 480 ) ) {
                isDevice = hasNumericalSuffix( type.match( /\d+$/g ) , "4" );
            }
        }
        if( isDevice ) {
            callback( orientation , callbackOnDevice.width , callbackOnDevice.height );
        }
    }

    // Ensure that network Object exists
    SEP.network = ( SEP.network ) ? SEP.network : {};

    SEP.network.setConnectivity = setConnectivity;
    SEP.network.setConnectionType = setConnectionType;
    SEP.network.callbackOnConnection = callbackOnConnection;

    // Ensure that device Object exists
    SEP.device = ( SEP.device ) ? SEP.device : {};
    SEP.device.callbackOnDevice = callbackOnDevice;
} ( SEP || {} ) );