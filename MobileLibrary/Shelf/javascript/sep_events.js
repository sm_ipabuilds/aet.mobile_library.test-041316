// Custom javascript
// Created by Jon May X.X.X
/* global SEP */

( function( SEP ) {
    "use strict";

    var topics = {},
        subUid = -1;
    
    /*
        * @desc Publish or broadcast events of interest with a specific topic name and
        * arguments such as the data to pass along
        * Additionally, supports partial application
        * @param string topic - topic event to trigger
        * @param object args - data passed to subscribed function
        * @return object - success: this, context in which function is triggered
        * @return object - failure: false if topic is not subscribed to
    */
    function publish( topic , args ) {
        var aa = SEP.timer();
        // publish Function supports partial application.
        // For example, var aFunc = publish( function( topic , args ) { SM.log( arguments ); } );
        if( arguments.length === 1 && typeof topic === "function" ) {
            var func = topic;
            return function( topic , args ) {
                var aa = SEP.timer();
                if ( !topics[ topic ] ) {
                    return false;
                }
                var subscribers = topics[ topic ],
                    len = subscribers ? subscribers.length : 0;
                // args can be modified by func scope
                func.call( this , topic , args );
                // func.call( this , topic , args );

                while ( len-- ) {
                    subscribers[ len ].func( topic , args );
                }

                SEP.debug( "event: " + topic , "duration: " + SEP.timer( aa ) );
                return this;
            };
        
        }
        if ( !topics[ topic ] ) {
            return false;
        }

        var subscribers = topics[ topic ],
            len = subscribers ? subscribers.length : 0;

        while ( len-- ) {
            subscribers[ len ].func( topic , args );
        }

        SEP.debug( "event: " + topic , "duration: " + SEP.timer( aa ) );
        return this;
    }

    /*
        * @desc Subscribe to events of interest with a specific topic name and a callback function,
        * to be executed when the topic/event is observed
        * @param string topic - topic event to subscribe to
        * @param function func - callback function to be executed when triggered
        * @return string - token value required for unsubscribe
    */
    function subscribe( topic , func ) {
        if ( !topics[ topic ] ) {
            topics[ topic ] = [];
        }

        var token = ( ++subUid ).toString();
        topics[ topic ].push( {
            token: token,
            func: func
        } );
        return token;
    }

    /*
        * @desc Unsubscribe from a specific topic, based on a tokenized reference to the subscription
        * @param string token - token to identify topic to unsubscribe
        * @return string - success: token is returned
        * @return object - failure: this, context in which function is triggered
    */
    function unsubscribe( token ) {
        for ( var key in topics ) {
            if ( topics[ key ] ) {
                for ( var i = 0, len = topics[ key ].length; i < len; i++ ) {
                    if ( topics[ key ][ i ].token === token ) {
                        topics[ key ].splice( i , 1 );
                        return token;
                    }
                }
            }
        }
        return this;
    }

    SEP.events = SEP.events || {};
    SEP.events.publish = publish;
    SEP.events.subscribe = subscribe;
    SEP.events.unsubscribe = unsubscribe;
} ( SEP || {} ) );