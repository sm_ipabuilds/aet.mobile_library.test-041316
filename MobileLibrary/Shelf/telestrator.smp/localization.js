/* global SM */

var translate = ( function( supportedLanguages , localizableStrings ) {
    "use strict";
    
    var appLanguage = SM.getProperty( "#systemLanguageManager" , "applicationLanguage" );
    
	function getLanguageAndRegion(str){
	    var comps = str.split("-");
	    if(comps.length > 2){
	        var sub = comps.slice(0,2);
	        str = sub.join("-");
	        return str;
	    }
	    return str;
	}    
    
    function getSupportedLanguage(langs){
        langs = langs || supportedLanguages;
        
        if(langs.indexOf(appLanguage) >= 0){
            return appLanguage;
        }
        
        var languageAndRegion = getLanguageAndRegion(appLanguage);
        if(langs.indexOf(languageAndRegion) >= 0){
            return languageAndRegion;
        }      
        
		var languageDesignatorOnly = appLanguage.substring(0,2);             
        if(langs.indexOf(languageDesignatorOnly) >= 0){
            return languageDesignatorOnly;
        }      		
        return "en";
    }    

    return {
        language: appLanguage,
        forString: function( string ) {
            var currentLanguage = getSupportedLanguage();                
            if( localizableStrings && localizableStrings[ currentLanguage ] && localizableStrings[ currentLanguage ][ string ] ) {
                return localizableStrings[ currentLanguage ][ string ];
            }
            return string;
        },
        fileWithPath: function( pathString , toolLanguages ) {               
            var localLanguages = toolLanguages || supportedLanguages;
            var currentLanguage = getSupportedLanguage(localLanguages);
            if( currentLanguage === "en" || localLanguages.indexOf( currentLanguage ) < 0 ) {
                return pathString;
            }

            var path = pathString.split( "/" ),
                file = path.pop().split( "." ),
                len = file.length,
                state = len >= 2 ? file[ len - 2 ].split( "-" ) : [];
            if( len === 1 || ( file[ 0 ] === "" && len === 2 ) ) {
                return pathString;
            }
            if( len >= 2 ) {
                if( state.length >= 2 ) {
                    state[ state.length - 2 ] += currentLanguage.toUpperCase();
                    file[ len - 2 ] = state.join( "-" );
                } else {
                    file[ len - 2 ] += currentLanguage.toUpperCase();
                }
            }
            path = path.length >= 1 ? path.join( "/" ) + "/" : "";
            return path + file.join( "." );
        },
        forSupportedLanguage: function( defValue , localizedValues ) {       
            defValue = defValue || localizedValues.en;
            var currentLanguage = getSupportedLanguage();
            if( localizedValues[ currentLanguage ] ) {
                return localizedValues[ currentLanguage ];
            }
            return defValue;
        }
    };
} ( SM.getArguments().properties.supportedLanguages || [] , {
    "en": {
        "drawButton": "drawButton", // Button asset
        "drawOnButton": "drawOnButton" // Button asset
    },
    "es": {
        "drawButton": "drawButtonES", // Button asset
        "drawOnButton": "drawOnButtonES" // Button asset
    },
    "fr": {
        "drawButton": "drawButtonFR", // Button asset
        "drawOnButton": "drawOnButtonFR" // Button asset
    },
    "zh-Hant": {
        "drawButton": "drawButtonZH-HANT", // Button asset
        "drawOnButton": "drawOnButtonZH-HANT" // Button asset
    }    
} ) );