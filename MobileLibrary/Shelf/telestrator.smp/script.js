// defaults.js
/* global SM, config , translate */

( function( global ) {
    "use strict";
	var parameters = SM.getArguments();
    global[ parameters.instanceId ] = {};

    var localizedDrawCloseBtn = translate.forString( "drawOnButton" ),
        defaultPackage = ( config.properties.presentationToolbarId ) ? "../" + SM.getActivePackageId() + "/" : "";

    var launchBtnId = SM.generateUniqueId(),
        canvasId = SM.generateUniqueId(),
        canvasCloseBtnId = SM.generateUniqueId(),
        emailId = SM.generateUniqueId(),
        isTelestrating = false;

    var launchBtn = {
        "overlayId": launchBtnId,
        "instanceId": SM.generateUniqueId(),
        "type": "button",
        "relative": config.relative,
        "layouts": config.layouts,
        "images": [ config.properties.image ],
        "imagesDown": [ config.properties.imageDown ],
        "actions": [
            {
                "action": "telestrateAction",
                "trigger": "touchUpInside",
                "target": config.instanceId,
                "data": {}
            }
        ]
    };

    function addEmailToContentSpec() {
        var contentSpec = { "overlays": {} },
        emailOverlay = {
            "type": "Email",
            "subject": SM.getActivePackageId(),
            "text": "Check out my Colorful Moment creation from the MobileLibrary!"
        };

        contentSpec.overlays[ emailId ] = emailOverlay;
        SM.addContentSpecModule( contentSpec , config.instanceId );
    }
    function spawnTelestrate() {
        isTelestrating = true;

        SM.spawn( {
            "overlayId": canvasId,
            "type": "canvas",
            "relative": "screen",
            "persistence": "package",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "x": "50%",
                    "y": "50%"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "x": "50%",
                    "y": "50%"
                }
            }
        } );

        SM.spawn( {
            "overlayId": canvasCloseBtnId,
            "type": "button",
            "relative": "screen",
            "persistence": "package",
            "images": [ defaultPackage + parameters.plugin + "/assets/" + localizedDrawCloseBtn + ".png" ],
            "imagesDown":[ defaultPackage + parameters.plugin + "/assets/" + localizedDrawCloseBtn + "-tap.png" ],
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "x":"70px",
                    "y":"50%"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "x":"70px",
                    "y":"50%"
                }
            },
            "draggable": true,
            "actions": [
                {
                    "action": "closeTelestrateAction",
                    "trigger": "touchUpInside",
                    "target": config.instanceId
                }
            ]
        } );
        SM.runAction( {
            "action": "setBrushColorAction",
            "trigger": "now",
            "target": canvasId,
            "data":{
                "color": config.properties.brushColor
            }
        } );
        SM.runAction( {
            "action": "setBrushWidthAction",
            "trigger": "now",
            "target": canvasId,
            "data": {
                "width": config.properties.brushSize
            }
        } );
    }

    //-- Actions --//
    global[ parameters.instanceId ].handleAction = function( action , data ) {
        if( action.toLowerCase() === "enabletoolaction" ) {
            // enableToolAction required for presentation toolbar
            spawnTelestrate();
            SM.runAction( {
                "action": "hideToolbarAction",
                "trigger": "now",
                "target": config.properties.presentationToolbarId
            });
        } else if( action.toLowerCase() === "finishloadingaction" ) {

        } else if( action.toLowerCase() === "telestrateaction" ) {
            spawnTelestrate();
        } else if( action.toLowerCase() === "closetelestrateaction") {
            isTelestrating = false;
            SM.runAction( {
                "action": "close",
                "trigger": "touchUpInside",
                "targets": [
                    canvasCloseBtnId,
                    canvasId
                ]
            } );
        } else if( action.toLowerCase() === "setbrushwidthaction") {
            SM.runAction( {
                "action": "setBrushWidthAction",
                "trigger": "now",
                "target": canvasId,
                "data": {
                    "width": data.width
                }
            } );

        } else if( action.toLowerCase() === "setbrushcoloraction" ) {
            SM.runAction( {
                "action": "setBrushColorAction",
                "trigger": "now",
                "target": canvasId,
                "data":{
                    "color": data.color
                }
            } );
        } else if(  action.toLowerCase() === "addemailoverlayaction" ) {
            addEmailToContentSpec();
        } else if( action.toLowerCase() === "updateforlayoutchange" ) {}

     };

    global[ parameters.instanceId ].getOutput = function( name ) {
        if( name.toLowerCase() == "myOutput" ) {
            return "My Output";
        }
    };
    
    //The close method is called when a plugin overlay is closed.
    global[ parameters.instanceId ].close = function() {
        //Remove the plugin spec from the main content.
        // SM.removeContentSpecModule( parameters.instanceId );
    };

    // Tool should be added to presentation toolbar
    if( config.properties.presentationToolbarId ) {
        // Tool is not in the presentation toolbar
        if( !config.properties.isInToolbox ) {
            // Close the original plugin overlay
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "target": parameters.instanceId
            } );

            // Add tool to presentation toolbar
            SM.runAction( {
                "action": "addToolToBoxAction",
                "trigger": "now",
                "target": config.properties.presentationToolbarId,
                "data": {
                    "title": "Draw",
                    "tool": config
                }
            } );
            
        }
    } else {
        SM.spawn( launchBtn );
    }
 } ( this ) );