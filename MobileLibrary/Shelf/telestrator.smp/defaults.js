// Default Plugin parameters
/* global SM */

var config = (function( params ) {
    function isObject( obj ) {
        return ( typeof obj === "object" ) ? true : false;
    }
    function duplicateJSON( object ) {
        var dup = (object instanceof Array) ? [] : {},
            key;
        for( key in object ) {
            if ( object[key] instanceof Array || object[key] instanceof Object ) {
                dup[key] = duplicateJSON(object[key]);
            } else {
                dup[key] = object[key];
            }
        }
        return dup;
    }
    function mergeConfig(defaultConfig, userConfig) {
        var merged = duplicateJSON( defaultConfig );
        var key;

        for(key in userConfig) {
            if( !isObject( userConfig[key] ) ) {
                merged[key] = userConfig[key];
            } else {
                merged[key] = mergeConfig( merged[key] , userConfig[key] );
            }
        }
        return merged;
    }

    var defaults = {
        "overlayId": SM.generateUniqueId(),
        "instanceId": "",
        "persistence": "page",
        "relative": "screen",
        "layouts": {
            "landscape": {
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "50%",
                "y": "50%"
            },
            "portrait": {
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "50%",
                "y": "50%"
            }
        },
        "properties": {
            "isInToolbox": false,
            "presentationToolbarId": "",
            "brushSize": "5",
            "brushColor": "#007AC2"
        }
    };
    return mergeConfig( defaults , params );
} ( SM.getArguments() ) );