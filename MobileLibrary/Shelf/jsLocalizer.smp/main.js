(function(global) {
    // Set the paramaters variable to the plugin contentSpec object to access its properties.
    var paramaters = SM.getArguments(); 
    
    // Create a global object to represent your plugin. Namespaced by the overlays instanceId.    
    global[paramaters.instanceId] = {};        
    var plugin = global[paramaters.instanceId];
    
    plugin.resetStringsTable = function(clear, table) {
    	if(clear === true)
    	{
			Localizer.clearStringsTable();
    	}
    	
		for(var propertyName in table) 
		{
			var subTable = table[propertyName];
			if(typeof(subTable) === 'object') 
			{
				Localizer.setStringsTable(propertyName, subTable);			
			}
		}    
    }
        
    //setup the initial language tables from arguments
    plugin.resetStringsTable(true, paramaters.properties['stringsTable']);

    //actions
	plugin.handleAction = function(action, data) {
        if(action == "setStringsTableAction")
        {
			plugin.resetStringsTable(true, data['stringsTable']);
        }
        else if(action == "appendStringsTableAction")
        {
			plugin.resetStringsTable(false, data['stringsTable']);
        } 
	};

	//outputs
    plugin.getOutput = function(name) {
		if(name.indexOf("{") === 0)
		{
			var prop = JSON.parse(name);			
			return Localizer.localizedString(prop.string);
		}              
    };
 }(this));
