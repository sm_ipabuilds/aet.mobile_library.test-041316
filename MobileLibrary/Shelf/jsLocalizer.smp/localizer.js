var Localizer = (function() {

	var stringsTable = {};
	
	var naiveCopy = function(object)
	{
		return JSON.parse(JSON.stringify(object));
	};

	var mergeObject = function(dest, src)
	{
		for(var propertyName in src) 
		{
			dest[propertyName] = naiveCopy(src[propertyName]);
		}	
	};
	
	var clearStringsTable = function(language, table)
	{
		stringsTable = {};	
	}

	var setStringsTable = function(language, table)
	{
		stringsTable[language] = {};
		stringsTable[language] = naiveCopy(table);
	};
	
	var getLanguageAndRegion = function(str){
	    var comps = str.split("-");
	    if(comps.length > 2){
	        var sub = comps.slice(0,2);
	        var str = sub.join("-");
	        return str;
	    }
	    return str;
	}
	
	var findString = function(str, lang){
	    if(stringsTable[lang] && stringsTable[lang][str]){
	        var localized = stringsTable[lang][str];
	        return localized;
	    }
	    return null;
	}

	var localizedString = function(str) 
	{
		//not sure if caching this will be more efficient or not..
		var currentLang = SM.getProperty("#systemLanguageManager", "applicationLanguage");
        var localizedStr = findString(str, currentLang);
        if(localizedStr){
            return localizedStr;
        } 

		var languageAndRegion = getLanguageAndRegion(currentLang);        
        localizedStr = findString(str, languageAndRegion);
        if(localizedStr){
            return localizedStr;
        }
        
		var languageDesignatorOnly = currentLang.substring(0,2);        
        localizedStr = findString(str, languageDesignatorOnly);
        if(localizedStr){
            return localizedStr;
        }  
        
        return str;       
	};
	
	return {
        localizedString: localizedString,
        setStringsTable: setStringsTable,
        clearStringsTable: clearStringsTable
     };
}());
	
	

