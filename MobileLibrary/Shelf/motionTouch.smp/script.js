// defaults.js
/* global SM, config */

( function( global ) {
    "use strict";
	var parameters = SM.getArguments();
    global[ parameters.instanceId ] = {};

    var launchBtnId = SM.generateUniqueId(),
        pointerId = SM.generateUniqueId(),
        isActive = false,
        defaultPackage = ( config.properties.presentationToolbarId ) ? "../" + SM.getActivePackageId() + "/" : "";

    var launchBtn = {
        "overlayId": launchBtnId,
        "instanceId": config.instanceId,
        "type": "button",
        "relative": config.relative,
        "layouts": config.layouts,
        "images": [ config.properties.image ],
        "imagesDown": [ config.properties.imageDown ],
        "borderWidth": "1px",
        "actions": [
            {
                "action": "motionTouchAction",
                "trigger": "touchUpInside",
                "target": config.instanceId,
                "data": {}
            }
        ]
    };


    function activate() {
        if( !isActive ) {
            isActive = true;
            SM.spawn( {
                "overlayId": pointerId,
                "type": "image",
                "relative": "touch",
                "persistence": "package",
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "100px",
                "y": "100px",
                "hidden": true,
                "clickThrough": true,
                "images": [ defaultPackage + config.plugin + "/assets/pointer_btn.png" ],
                "actions": [
                    {
                        "action": "setHiddenAction",
                        "trigger": "relativeTouchBegan",
                        "target": pointerId,
                        "data": {
                            "hidden": false
                        }
                    },
                    {
                        "action": "bringToFront",
                        "trigger": "relativeTouchBegan",
                        "delay": 0.1,
                        "target": pointerId
                    },
                    {
                        "action": "setHiddenAction",
                        "trigger": "relativeTouchEnded",
                        "target": pointerId,
                        "data": {
                            "hidden": true
                        }
                    }
                ]
            } );
        } else {
            isActive = false;
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "target": pointerId
            } );
        }
        
    }

    //-- Actions --//
    global[ parameters.instanceId ].handleAction = function( action , data ) {
        if( action.toLowerCase() === "enabletoolaction" ) {
            // enableToolAction required for presentation toolbar
            activate();
            SM.runAction( {
                "action": "hideToolbarAction",
                "trigger": "now",
                "target": config.properties.presentationToolbarId
            });
        } else if( action.toLowerCase() === "motiontouchaction" ) {
            activate();
        } else if( action.toLowerCase() === "updateforlayoutchange" ) {}

        //Route all other actions directed to the plugin to the plugin Container.
        else {
            //iOS client can't handle a null value, so we set undefined data to {};
            if( !data ) { data = {}; }
            SM.runAction( {
                "action": action,
                "target": config.overlayId,
                "trigger": "now",
                "data": data
            } );
        }
     };

    global[ parameters.instanceId ].getOutput = function( name ) {
        if( name.toLowerCase() == "myOutput" ) {
            return "My Output";
        }
    };

    //The close method is called when a plugin overlay is closed.
    global[parameters.instanceId].close = function() {
        //Remove the plugin spec from the main content.
        // SM.removeContentSpecModule(parameters.instanceId);        
    };

    // Tool should be added to presentation toolbar
    if( config.properties.presentationToolbarId ) {
        // Tool is not in the presentation toolbar
        if( !config.properties.isInToolbox ) {
            // Close the original plugin overlay
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "target": parameters.instanceId
            } );

            // Add tool to presentation toolbar
            SM.runAction( {
                "action": "addToolToBoxAction",
                "trigger": "now",
                "target": config.properties.presentationToolbarId,
                "data": {
                    "tool": config,
                    "title": "motion touch"
                }
            } );
        }
    } else {
        SM.spawn( launchBtn );
    }
 } ( this ) );