// Default Plugin parameters
/* global SM */

var config = ( function( params ) {
    "use script";
    function isObject( obj ) {
        return ( typeof obj === "object" ) ? true : false;
    }
    function duplicateJSON( object ) {
        var dup = ( object instanceof Array ) ? [] : {},
            key;
        for( key in object ) {
            if ( object[ key ] instanceof Array || object[ key ] instanceof Object ) {
                dup[ key ] = duplicateJSON( object[ key ] );
            } else {
                dup[ key ] = object[ key ];
            }
        }
        return dup;
    }
    function mergeConfig( defaultConfig , userConfig ) {
        var merged = duplicateJSON( defaultConfig );
        var key;

        for( key in userConfig ) {
            if( !isObject( userConfig[ key ] ) ) {
                merged[ key ] = userConfig[ key ];
            } else {
                merged[ key ] = mergeConfig( merged[ key ] , userConfig[ key ] );
            }
        }
        return merged;
    }

    var defaults = {
        "overlayId": SM.generateUniqueId(),
        "instanceId": "",
        "relative": "screen",
        "properties": {
            "defaultPackage": SM.getActivePackageId(),
            "showHelpOnAppletLoad": true,
            "disableToolbar": false
        }
    };
    return mergeConfig( defaults , params );
} ( SM.getArguments() ) );


// Create Custom Event Listener
var pubsub = ( function() {
    "use script";
    var topics = {},
        subUid = -1;

    // Publish or broadcast events of interest
    // with a specific topic name and arguments
    // such as the data to pass along
    function publish( topic , args ) {

        if ( !topics[ topic ] ) {
            return false;
        }

        var subscribers = topics[ topic ],
            len = subscribers ? subscribers.length : 0;

        while ( len-- ) {
            subscribers[ len ].func( topic , args );
        }

        return this;
    }

    // Subscribe to events of interest
    // with a specific topic name and a
    // callback function, to be executed
    // when the topic/event is observed
    function subscribe( topic , func ) {

        if ( !topics[ topic ] ) {
            topics[ topic ] = [];
        }

        var token = ( ++subUid ).toString();
        topics[ topic ].push( {
            token: token,
            func: func
        } );
        return token;
    }

    // Unsubscribe from a specific
    // topic, based on a tokenized reference
    // to the subscription
    function unsubscribe( token ) {
        for ( var key in topics ) {
            if ( topics[ key ] ) {
                for ( var i = 0, len = topics[ key ].length; i < len; i++ ) {
                    if ( topics[ key ][ i ].token === token ) {
                        topics[ key ].splice( i , 1 );
                        return token;
                    }
                }
            }
        }
        return this;
    }

    return {
        publish: publish,
        subscribe: subscribe,
        unsubscribe: unsubscribe
    };
} () );

/* Per-user settings stored on the native client. */

var userSettings = ( function() {
    return {
        getSetting: function( key ) {
            var propString = "userSettings." + key;
            return SM.getProperty( "#systemPackageDataRegistry" , propString );
        },
        setSetting: function( key , value ) {
            var action = {
                "action":"saveUserSettingAction",
                "target":"#systemPackageDataRegistry",
                "trigger":"now",
                "data": {
                    "settingKey":key,
                    "settingValue":value
                }
            };
            SM.runAction( action );
        }
    };
}() );

/* Used to determine if an object exists in a JSON array. */

var sepUtils = ( function() {
    return {
        arrayContains: function( array , obj ) {
            for( var item in array ) {
                SM.log( array[ item ] );
                if( array[ item ] === obj ) {
                    return true;
                }
            }
            return false;
        },
        copyProps:function( src , dest ) {
            for( var key in src ) {
                dest[ key ] = src[ key ];
            }
        },
        arrayFromJson:function( src ) {
            var array = [];
            
            for( var key in src ) {
                array.push( src[ key ] );
            }
            return array;
        }
    };
}() );