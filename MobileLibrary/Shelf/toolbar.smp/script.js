// defaults.js, events.js, animate.js, layouts.js
/* global SM , toolbox , config , layouts , pubsub , generateOverlays , helpLayout , userSettings */

( function( global ) {
    "use strict";
    var parameters = SM.getArguments();
    global[ parameters.instanceId ] = {};

    var lastPackage = "";

    var pageChanged = SM.generateUniqueId(),
        packageChanged = SM.generateUniqueId(),
        
        // Boolean for disabling toolbar action
        isToolbarDisabled = config.properties.disableToolbar,
        showHelpOnAppletLoad = config.properties.showHelpOnAppletLoad,
        overlays = generateOverlays(),
        isHidden = true;
        
    //get overlayIds from overlays
        
    var toolbarId = overlays.toolbar.overlayId,
        toolbarChildId = overlays.toolbarChild.overlayId,
        toolbarBkgdId = overlays.toolbarBkgd.overlayId,
        toolbarBtnId = overlays.toolbarBtn.overlayId;
        
    // Check if active packageId is defaultPackage
    function isDefaultPackage() {
        if( SM.getActivePackageId() === config.properties.defaultPackage ) {
            return true;
        }
    }

    // setHiddenAction function for convenience 
    function setHiddenAction( overlayIds , hiddenBool , delay ) {
        SM.runAction( {
            "action": "setHiddenAction",
            "trigger": "now",
            "delay": delay || 0,
            "targets": overlayIds,
            "data": {
                "hidden": hiddenBool
            }
        } );
    }

    function moveToWindow( location ) {
        // Bring to front of window for screen mirroring
        // Bring to front of window for persistent overlays
        var action = {
            "trigger": "now",
            "targets": [ toolbarBtnId , toolbarBkgdId , toolbarId ]
        };
        if( location === "front" ) {
            action.action = "bringToFrontWindow";
        } else if( location === "back" ) {
            action.action = "sendToBackWindow";
        }
        
        if( action.action ) { SM.runAction( action ); }
        SM.runAction( {
            "action": "bringToFront",
            "trigger": "now",
            "targets": [ toolbarBtnId , toolbarBkgdId , toolbarId ]
        } );
    }

    // Exit to the Shelf packageId
    function exitToShelf() {
        toolbox.removeAppletOverlays();
        setToolbarHidden( true , 0.1 );
        SM.runAction( {
            "action": "#loadDefaultPackage",
            "trigger": "now",
            "data": {
                "packageId": config.properties.defaultPackage,
                "transitionType": "pushout",
                "transitionDuration": 0.3
            }
        } );
    }
    
    // set toolbar show and hide animations for convenience
    function setToolbarHidden( hiddenBool , aDuration ) {
        if( !setToolbarHidden.animPosId ) {
            setToolbarHidden.animPosId = "";
        }
        if( !setToolbarHidden.animAlphaId ) {
            setToolbarHidden.animAlphaId = "";
        }

        // Animation properties
        var delay = 0,
            duration = aDuration || 0.5,
            activeX,
            activeY,
            restX,
            restY,
            
            restAlpha,
            activeAlpha;

        // At this time the animations are defined for landscape
        restX = layouts.animate.landscape.fromX;
        restY = layouts.animate.landscape.fromY;
        activeY = layouts.animate.landscape.toY;
        activeX = layouts.animate.landscape.toX;
        restAlpha = 0;
        activeAlpha = 1;

        // Animate here for convenience
        // Toolbar is hidden and action is to show
        if( hiddenBool === false && isHidden !== hiddenBool ) {
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": toolbarId,
                data: {
                    "animation": {
                        "type": "Animation",
                        "properties": [
                            {
                                "property": "x",
                                "fromValue": restX,
                                "toValue": activeX,
                                "duration": duration
                            },
                            {
                                "property": "y",
                                "fromValue": restY,
                                "toValue": activeY,
                                "duration": duration
                            }
                        ]
                    }
                }
            } );

            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": toolbarBkgdId,
                data: {
                    "animation": {
                        "type": "Animation",
                        "properties": [
                            {
                                "property": "alpha",
                                "fromValue": restAlpha,
                                "toValue": activeAlpha,
                                "duration": duration
                            }
                        ]
                    }
                }
            } );

            moveToWindow( "front" );
        }
        // Toolbar is visible and action is to hide
        else if( hiddenBool === true && isHidden !== hiddenBool ) {
            // Delay until animation is complete
            delay = duration;
            
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": toolbarId,
                data: {
                    "animation": {
                        "type": "Animation",
                        "properties": [
                            {
                                "property": "x",
                                "fromValue": activeX,
                                "toValue": restX,
                                "duration": duration
                            },
                            {
                                "property": "y",
                                "fromValue": activeY,
                                "toValue": restY,
                                "duration": duration
                            }
                        ]
                    }
                }
            } );

            SM.runAction( {
                "action":"animate",
                "trigger":"now",
                "target":toolbarBkgdId,
                data: {
                    "animation": {
                        "type": "Animation",
                        "properties": [
                            {
                                "property": "alpha",
                                "fromValue": activeAlpha,
                                "toValue": restAlpha,
                                "duration": duration
                            }
                        ]
                    }
                }
            } );

            moveToWindow( "back" );
        }
        // Attempting to animate with conflict
        else if( hiddenBool === true && isHidden === hiddenBool ) {
            // Assume conflicting with toolbar hidden
            return;
        }
        // Attempting to animate with conflict
        else {
            // Assume conficting with toolbar visible
            isHidden = true;
            return;
        }
        
        setToolbarHidden.isAnimate = new Date();
        isHidden = hiddenBool;
        setHiddenAction( [ toolbarId , toolbarBkgdId ] , hiddenBool , delay );
    }
    
    function showHelpOverlay( show ) {
        SM.log("ShowHelp");
        
        if( show ) {
            SM.spawn( overlays.helpOverlay );
        
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": overlays.innerHelpContainerId,
                "data": {
                    "animation": helpLayout.inAnimation
                }
            } );
        } else {
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": overlays.innerHelpContainerId,
                "data": {
                    "animation":helpLayout.outAnimation
                }
            } );
              
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "target": overlays.helpOverlay.overlayId,
                "delay":helpLayout.outAnimation.properties[ 0 ].duration
            } );
        }
    }
    
    /******************** EVENTS ********************/

    // Define event actions
    function eventConditions( topics , data ) {
        //SM.log( "Logging: " + topics + ": " + data );         
        
        if( topics === "packageChangedEvent" ) {
            // Active package is default package
            
            var currentPackage = SM.getActivePackageId(),
                wasDisplayedKey,
                wasDisplayed;
            
            if( isDefaultPackage() ) {
                setToolbarHidden( true );
                isToolbarDisabled = false;
                setHiddenAction( [ toolbarBtnId ] , true , 0 );
            }

            // Active package is NOT default package
            // Don't show if we're showing after an update
            else if( lastPackage !== "__INSTALL_VIEW_PATH__" && currentPackage !== "__INSTALL_VIEW_PATH__" && currentPackage !== lastPackage ) {
                setHiddenAction( [ toolbarBtnId ] , false , 0 );

                //display help overlay if needed
                wasDisplayedKey = "sep_toolbar_was_displayed";
                wasDisplayed = userSettings.getSetting( wasDisplayedKey );
        
                if( wasDisplayed !== "true" && showHelpOnAppletLoad === true && !isToolbarDisabled ) {
                    showHelpOverlay( true );
                }
            }
                      
            lastPackage = currentPackage;
            
        } else if( topics === "pageChangedEvent" ) {
        }
    }

    /****************** END EVENTS ******************/

    // Spawn Additional Overlays

    SM.spawn( overlays.toolbarBtn );
    SM.spawn( overlays.toolbarBkgd );
    SM.spawn( overlays.toolbar );
    SM.spawnInto( overlays.toolbar.overlayId , overlays.toolbarChild );

//-- Actions --//
    
    var actions = {};
    // The below actions WORK AS EXPECTED though very backwards
    actions[ pageChanged ] = {
		"action": "triggerEventAction",
		"trigger": "now",
		"target": config.instanceId,
		"data": {
			"event": "pageChanged"
		}
	};

    actions[ packageChanged ] = {
		"action": "triggerEventAction",
		"trigger": "now",
		"target": config.instanceId,
		"data": {
			"event": "packageChangedEvent"
		}
	};

    SM.addContentSpecModule( { "actions": actions } , config.instanceId );

    global[ parameters.instanceId ].handleAction = function( action , data ) {
        function sanitize( tool ) {
            if( tool.layouts ) { delete tool.layouts; }
            if( tool.actions ) { delete tool.actions; }
            if( tool.overlays ) { delete tool.overlays; }
        }
        function configure( tool ) {
            // Set persistence
            if( isDefaultPackage() ) {
                tool.persistence = "application-permanent";
            } else {
                tool.persistence = "package";
            }
            // Redefine instanceId
            tool.instanceId = SM.generateUniqueId();
            // Tool will be added to presentation toolbar
            tool.properties.isInToolbox = true;

            // Load action
            tool.actions = [ {
                "action": "finishLoadingAction",
                "trigger": "now",
                "target": tool.overlayId
            } ];
            return tool;
        }

        if( action.toLowerCase() === "finishloadingaction" ) {
            //subscribe to events with confidence now that the plugin as truly finished loading
            data = {
				"event": "pageChangedEvent",
				"actionId": pageChanged
			};

		    pubsub.subscribe( data.event , eventConditions );
		    SM.runAction( {
				"action": "subscribe",
				"target": "#eventManager",
				"trigger": "now",
				"data": {
					"event": data.event,
					"actionId": data.actionId
				}
			} );
			
			data = {
				"event": "packageChangedEvent",
				"actionId": packageChanged
			};

			pubsub.subscribe( data.event , eventConditions );
		    SM.runAction( {
				"action": "subscribe",
				"target": "#eventManager",
				"trigger": "now",
				"data": {
					"event": data.event,
					"actionId": data.actionId
				}
			} );
        } else if( action.toLowerCase() === "triggereventaction" ) {
            if( data && data.event ) {
                pubsub.publish( data.event , "Plugins!" );
            }
        } else if( action.toLowerCase() === "exitaction" ) {
            SM.log( "exit to shelf" );
            exitToShelf();
        } else if( action.toLowerCase() === "hidetoolbaraction" ) {
            // hide if toolbar enabled
            if( !isToolbarDisabled ) { setToolbarHidden( true ); }
        } else if( action.toLowerCase() === "showtoolbaraction" ) {
            // show if toolbar enabled
            if( !isToolbarDisabled ) { setToolbarHidden( false ); }
        } else if( action.toLowerCase() === "disabletoolbaraction" ) {
            isToolbarDisabled = true;
        } else if( action.toLowerCase() === "enabletoolbaraction" ) {
            isToolbarDisabled = false;
        } else if( action.toLowerCase() === "addtooltoboxaction" ) {
            if( data.tool ) {
                var tool = data.tool;
                // Sanitize the plugin Object
                sanitize( tool );

                // Spawn reconfigured plugin overlay
                SM.spawn( configure( tool ) );

                // Add plugin properties required for toolbar
                tool.toolId = tool.overlayId;
                // Touch triggers only
                data.actions = [
                    {
                        "action": "enableToolAction",
                        "trigger": "touchUpInside",
                        "target": tool.toolId
                    }
                ];
                toolbox.addToolOverlay( data , toolbarChildId );
            }
        } else if( action.toLowerCase() === "updateforlayoutchange" ) {
            setToolbarHidden( true );
        } else if( action.toLowerCase() === "closehelp" ) {
            showHelpOverlay( false );
        }
     };

    global[ parameters.instanceId ].getOutput = function( name ) {
        if( name.toLowerCase() == "myOutput" ) {
            return "";
        }
    };

    global[ parameters.instanceId ].close = function() {};
 } ( this ) );