// Layouts Plugin parameters
/* global SM , translate */

var layouts = ( function() {
	"use strict";
	var layouts = makeLayouts( {
            "horizontalAlign": "left",
            "verticalAlign": "top",
            "x": "0px",
            "y": "0px",
            "width": "155px"
        } ),
		animate = {};

    callbackOnDevice( "any" , function( orientation , width , height ) {
        layouts = layouts.overwrite( "landscape" , {
            "height": width + "px"
        } ).overwrite( "portrait" , {
            "height": height + "px"
        } ).build();
    } );

    animate.landscape = {
        "fromX": -1 * parseFloat( layouts.landscape.width ),
        "fromY": 0,
        "toX": parseFloat( layouts.landscape.x ),
        "toY": 0
    };
    animate.portrait = {
        "fromX": -1 * parseFloat( layouts.portrait.width ),
        "fromY": 0,
        "toX": parseFloat( layouts.portrait.x ),
        "toY": 0
    };

    function callbackOnDevice( type , callback ) {
        callbackOnDevice.width = callbackOnDevice.width || 0;
        callbackOnDevice.height = callbackOnDevice.height || 0;
        var screenSize,
            orientation = SM.getOrientation(),
            isDevice = false,
            isDeviceType = function( type ) {
                var regex = this.match( new RegExp( type , "g" ) );

                if( regex && regex.toString().toLowerCase() === type ) {
                    return true;
                }
                return false;
            },
            hasNumericalSuffix = function( suffix ) {
                var args =  Array.prototype.slice.call( arguments ).slice( 1 ),
                    hasSuffix = false,
                    key;
                if( !suffix ) { return true; } // Return true if no suffix
                for( key in args ) {
                    if( suffix[ 0 ] === args[ key ] ) {
                        hasSuffix = true;
                    }
                }
                return hasSuffix;
            },
            hadDeviceSize = function( size ) {
                return ( size === callbackOnDevice.width || size === callbackOnDevice.height ) ? true : false;
            };

        if( !callbackOnDevice.width && !callbackOnDevice.height ) {
            screenSize = SM.getScreenSize();
            
            // iOS 8 SDK changes the behavior of SM.getScreenSize
            // callbackOnDevice.width = screenSize.width;
            // callbackOnDevice.height = screenSize.height;

            // Normalize SM.getScreenSize
            /* Begin workaround to new behavior */
            if( orientation === "landscape" ) {
                // If iOS 8 SDK, width will be greater than height, else return size
                if( screenSize.width > screenSize.height ) { 
                    callbackOnDevice.width = screenSize.height;
                    callbackOnDevice.height = screenSize.width;
                } else {
                    callbackOnDevice.width = screenSize.width;
                    callbackOnDevice.height = screenSize.height;
                }
            } else {
                callbackOnDevice.width = screenSize.width;
                callbackOnDevice.height = screenSize.height;
            }
            /* End workaround */
        }

        if( type.toLowerCase() === "any" ) {
            isDevice = true;
        } else if( isDeviceType.call( type , "ipad" ) && hadDeviceSize( 1024 ) ) {
            isDevice = true;
        } else if( isDeviceType.call( type , "iphone" ) ) {
            if( hadDeviceSize( 568 ) ) {
                isDevice = hasNumericalSuffix( type.match( /\d+$/g ) , "5" );
            } else if( hadDeviceSize( 480 ) ) {
                isDevice = hasNumericalSuffix( type.match( /\d+$/g ) , "4" );
            }
        }
        if( isDevice ) {
            callback( orientation , callbackOnDevice.width , callbackOnDevice.height );
        }
    }
    
    function makeLayouts() {
        var context = {},
            layouts = { landscape: {} , portrait: {} },
            args = arguments,
            data = ( args.length === 1 && typeof args[ 0 ] === "object" ) ? args[ 0 ] : {};
        
        for( var key in data ) {
            if( isALayoutKey( key ) ) {
                layouts.landscape[ key ] = layouts.portrait[ key ] = data[ key ];
            }
        }
        
        function isALayoutKey( key ) {
            var layoutKeys = [ "horizontalAlign" , "verticalAlign" , "x" , "y" , "width" , "height" ];
            return ( layoutKeys.indexOf( key ) ) > -1 ? true : false ;
        }
        
        function overwrite( layout , data ) {
            layout = layouts[ layout ];
            if( layout === undefined ) { return context; }
            for( var key in data ) {
                if( isALayoutKey( key ) ) {
                    layout[ key ] = data[ key ];
                }
            }
            return context;
        }

        context.build = function() { return layouts; };
        context.overwrite = overwrite;
        return context;
    }

	return {
		layouts: layouts,
        makeLayouts: makeLayouts,
		animate: animate,
        callbackOnDevice: callbackOnDevice
	};
} () );

var btnLayout = ( function( helpers ) {
	"use strict";
	var layouts = helpers.makeLayouts( {
            "verticalAlign": "center",
            "horizontalAlign": "left",
            "x": "0px",
            "y": "50%",
            "width": "20px"
        } ),
        heightModifier = 160; // Height modifier reduces the size of the active swipe area

    helpers.callbackOnDevice( "any" , function( orientation , width , height ) {
        layouts = layouts.overwrite( "landscape" , {
            "height":  width - heightModifier + "px"
        } ).overwrite( "portrait" , {
            "height":  height - heightModifier + "px"
        } ).build();
    } );
	return {
		layouts: layouts
	};
} ( layouts ) );

var childLayout = ( function( helpers ) {
	"use strict";
    // TOOLBAR SUPPORTS DYNAMICAL TOOLS.
    // DEFECT WITH SETPOSITIONACTION REQUIRES VALUE TO BE HARDCODED.
	var toolsNum = 2, // VALUE IS HARDCODED.
        paddingHeight = 27, // vertical padding for flow container
        toolHeight = 106, // ideal tool height, this is not currently enforced
        toolOffset = 65, // top margin plus the height of the exit button
		layouts = helpers.makeLayouts( {
            "horizontalAlign": "center",
            "verticalAlign": "center",
            "x": "50%",
            "width": "140px"
        } );

    var layoutOptions = {
        "paddingX": "0px",
        "paddingY": "40px",
        "marginX": "0px",
        "marginY": "0px",
        "margin-top": "0px",
        "margin-bottom": "0px",
        "margin-left": "25px",
        "margin-right": "25px"
    };
    
    helpers.callbackOnDevice( "iphone" , function() {
        layoutOptions[ "margin-top" ] = "15px";
        layoutOptions.paddingY = "25px";
    } );

    helpers.callbackOnDevice( "any" , function( orientation , width , height ) {
        /* REQUIRES SETPOSITIONACTION TO WORK FOR DUAL ORIENTATION */

        // layouts = layouts.overwrite( "landscape" , {
        //     "y": ( width - toolOffset ) / 2 + toolOffset + "px",
        //     "height": width - toolOffset + "px"
        // } ).overwrite( "portrait" , {
        //     "y": ( height - toolOffset ) / 2 + toolOffset + "px",
        //     "height": height - toolOffset + "px"
        // } ).build();

        /* END SETPOSITIONACTION */

        /* WORK AROUND */

        var toolContentHeight = toolHeight * toolsNum + paddingHeight * ( toolsNum - 1 ),
            landscapeY = ( width - toolOffset ) / 2 + toolOffset,
            portraitY = ( height - toolOffset ) / 2 + toolOffset;
        
        if( toolContentHeight < width - toolOffset * 2 ) {
            landscapeY = width - ( toolOffset + toolContentHeight ) / 2;
        }
        if( toolContentHeight < height - toolOffset * 2 ) {
            portraitY = height - ( toolOffset + toolContentHeight ) / 2;
        }

        layouts = layouts.overwrite( "landscape" , {
            "y": landscapeY + "px",
            "height": width - toolOffset + "px"
        } ).overwrite( "portrait" , {
            "y": portraitY + "px",
            "height": height - toolOffset + "px"
        } ).build();

        /* END WORK AROUND */
    } );

	return {
		layouts: layouts,
        layoutOptions: layoutOptions
	};
} ( layouts ) );

var helpLayout = ( function( helpers ) {
	"use strict";
	var containerLayouts = helpers.makeLayouts( {
            "horizontalAlign":"center",
            "verticalAlign":"center",
            "height": translate.forSupportedLanguage( "360px" , { "fr": "382px" , "es": "382px" } ),
            "width": "360px",
            "x":"50%",
            "y":"50%"
        } ),
		inAnimation = {},
		outAnimation = {},

        titleTextLayouts = helpers.makeLayouts( {
            "horizontalAlign": "center",
            "verticalAlign": "top",
            "x": "50%",
            "y": "34px",
            "width": "330px",
            "height": "100px"
        } ),
        imgLayouts = helpers.makeLayouts( {
            "horizontalAlign": "center",
            "verticalAlign": "center",
            "x": "50%",
            "y": translate.forSupportedLanguage( "180px" , { "fr": "202px" , "es": "202px" } ),
            "width": "91px",
            "height": "104px"
        } ),
        acceptBtn1Layouts = helpers.makeLayouts( {
            "horizontalAlign": "left",
            "verticalAlign": "top",
            "x": "-1px",
            "y": translate.forSupportedLanguage( "261px" , { "fr": "283px" , "es": "283px" } ),
            "width": "362px",
            "height": "50px"
        } ),
        acceptBtn2Layouts = helpers.makeLayouts( {
            "horizontalAlign": "left",
            "verticalAlign": "top",
            "x": "-1px",
            "y": translate.forSupportedLanguage( "311px" , { "fr": "333px" , "es": "333px" } ),
            "width": "362px",
            "height": "50px"
        } );

    helpers.callbackOnDevice( "ipad" , function() {
        containerLayouts = containerLayouts.build();
        titleTextLayouts = titleTextLayouts.build();
        imgLayouts = imgLayouts.build();
        acceptBtn1Layouts = acceptBtn1Layouts.build();
        acceptBtn2Layouts = acceptBtn2Layouts.build();
    } );
    helpers.callbackOnDevice( "iphone" , function() {
        containerLayouts = containerLayouts.overwrite( "landscape" , {
            "width": "284px",
            "height": translate.forSupportedLanguage( "240px" , { "fr": "262px" , "es": "262px" } )
        } ).overwrite( "portrait" , {
            "width": "284px",
            "height": translate.forSupportedLanguage( "360px" , { "fr": "386px" , "es": "386px" } )
        } ).build();
        titleTextLayouts = titleTextLayouts.overwrite( "landscape" , {
            "y": "24px",
            "width": "254px"
        } ).overwrite( "portrait" , {
            "y": "24px",
            "width": "254px"
        } ).build();
        
        // Not the best plan to hide image but still functional
        imgLayouts = imgLayouts.overwrite( "landscape" , {
            "x": "150%",
            "y": "150%",
            "width": "0px",
            "height": "0px"
        } ).overwrite( "portrait" , {
            "y": translate.forSupportedLanguage( "180px" , { "fr": "206px" , "es": "206px" } )
        } ).build();
        acceptBtn1Layouts = acceptBtn1Layouts.overwrite( "landscape" , {
            "y": translate.forSupportedLanguage( "141px" , { "fr": "163px" , "es": "163px" } ),
            "width": "286px"
        } ).overwrite( "portrait" , {
            "y": translate.forSupportedLanguage( "261px" , { "fr": "287px" , "es": "287px" } ),
            "width": "286px"
        } ).build();
        acceptBtn2Layouts = acceptBtn2Layouts.overwrite( "landscape" , {
            "y": translate.forSupportedLanguage( "191px" , { "fr": "213px" , "es": "213px" } ),
            "width": "286px"
        } ).overwrite( "portrait" , {
            "y": translate.forSupportedLanguage( "311px" , { "fr": "337px" , "es": "337px" } ),
            "width": "286px"
        } ).build();
    } );

    
    inAnimation = {
        "properties": [
            {
                "property": "alpha",
                "fromValue": "0.0",
                "toValue": "1.0",
                "duration": 0.5
            },
            {
                "property": "scale",
                "fromValue": "0.95",
                "toValue": "1.0",
                "duration": 0.5
            }
        ]
    };
    outAnimation = {
        "properties": [
            {
                "property": "alpha",
                "fromValue": "1.0",
                "toValue": "0.0",
                "duration": 0.5
            },
            {
                "property": "scale",
                "fromValue": "1.0",
                "toValue": "0.95",
                "duration": 0.5
            }
        ]
    };



	return {
		containerLayouts: containerLayouts,
		inAnimation: inAnimation,
		outAnimation: outAnimation,

        titleTextLayouts: titleTextLayouts,
        imgLayouts: imgLayouts,
        acceptBtn1Layouts: acceptBtn1Layouts,
        acceptBtn2Layouts: acceptBtn2Layouts
	};
} ( layouts ) );