// Presentation toolbar toolbox
/* global SM , config , layouts , translate */

var toolbox = ( function () {
    "use strict";
    var toolOverlays = [];

    // Check if active packageId is defaultPackage
    function isDefaultPackage() {
        if( SM.getActivePackageId() === config.properties.defaultPackage ) {
            return true;
        }
        return false;
    }

    // Index of tool overlay in toolOverlays Array
    function indexOfTool( toolId ) {
        var i = 0,
            len = toolOverlays.length;

        if( toolId ) {
            for( i; i < len; i++ ) {
                if( toolOverlays[ i ].toolId === toolId ) {
                    return i;
                }
            }
        }
        return -1;
    }

    // Add tool to toolOverlays Array based on defaultPackage
    function addToolOverlay( data , toolContainerId ) {
        var tool = data.tool,
            toolsNum,
            paddingHeight = 27, // vertical padding for flow container
            toolHeight = 106, // ideal tool height, this is not currently enforced
            toolOffset = 65; // top margin plus the height of the exit button
        // Require id and prevent duplicate tools by id
        if( !tool.toolId || indexOfTool( tool.toolId ) >= 0 ) {
            return;
        }

        // Generate new overlayId
        tool.overlayId = SM.generateUniqueId();
        if( isDefaultPackage() ) {
            toolOverlays.push( {
                "overlayId": tool.overlayId,
                "toolId": tool.toolId,
                "type": "shelf"
            } );
        } else {
            toolOverlays.push( {
                "overlayId": tool.overlayId,
                "toolId": tool.toolId,
                "type": "applet"
            } );
        }

        toolsNum = toolOverlays.length;

        SM.runAction( {
            "action": "spawn",
            "trigger": "now",
            "target": toolContainerId, //toolbarChildId,
            "data": createTool( data )
        } );

        /* REQUIRES SETPOSITIONACTION TO WORK FOR DUAL ORIENTATION */
        
        // layouts.callbackOnDevice( "any" , function( orientation , width , height ) {
        //     var toolContentHeight = toolHeight * toolsNum + paddingHeight * ( toolsNum - 1 ),
        //         landscapeY = ( width - toolOffset ) / 2 + toolOffset,
        //         portraitY = ( height - toolOffset ) / 2 + toolOffset;
            
        //     if( toolContentHeight < width - toolOffset * 2 ) {
        //         landscapeY = width - ( toolOffset + toolContentHeight ) / 2;
        //     }
        //     if( toolContentHeight < height - toolOffset * 2 ) {
        //         portraitY = height - ( toolOffset + toolContentHeight ) / 2;
        //     }
        //     SM.runAction( {
        //         "action": "setPositionAction",
        //         "trigger": "now",
        //         "target": toolContainerId,
        //         "data": {
        //             "layouts": {
        //                 "landscape": {
        //                     "x": "77.5px",
        //                     "y": landscapeY + "px"
        //                 },
        //                 "portrait": {
        //                     "x": "77.5px",
        //                     "y": portraitY + "px"
        //                 }
        //             }
        //         }
        //     } );
        // } );
        
        /* END SETPOSITIONACTION */
    }

    // Tool overlay defaults
    function createTool( data ) {
        var tool;
        if( data.tool ) {
            tool = data.tool;
            delete data.tool;
        }
        // Superficial Object merging
        function mergeData( defaultConfig , userConfig ) {
            var key;

            for( key in userConfig ) {
                    defaultConfig[ key ] = userConfig[ key ];
            }
        }

        // Tool button defaulting to text
        var button = {
            "overlayId": tool.overlayId,
            "type": "button",
            "relative": "parent",
            "text": data.title || "",
            "textAlign": "center",
            "scaleMode": "center",
            "fixedSize": true
        };

        mergeData( button , data );

        // Add button images
        if( tool.properties.image ) {
            button.images = [ translate.fileWithPath( tool.properties.image , tool.properties.supportedLanguages || [] ) ];
            button.width = "90px";
            button.height = "106px";
        }
        if( tool.properties.imageDown ) {
            button.imagesDown = [ translate.fileWithPath( tool.properties.imageDown , tool.properties.supportedLanguages || [] ) ];
        } else {
            button.imagesDown = [ "" ];
        }

        return button;
    }

    // Remove a tool
    // function removeToolOverlay( overlayId ) {
    //     var tool = indexOfTool( overlayId );
    //     tool = toolOverlays.splice( tool , 1 );
    //     SM.runAction( {
    //         "action": "close",
    //         "trigger": "now",
    //         "targets": tool[ 0 ].overlayId
    //     } );
    // }

    // Remove all applet tools
    function removeAppletOverlays() {
        var i = 0,
            len = toolOverlays.length,
            tools = [];

        for( i; i < len; i++ ) {
            if( toolOverlays[ i ].type === "applet" ) {
                tools.push( toolOverlays.splice( i , 1 )[ 0 ].overlayId );
                
            }
        }
        SM.runAction( {
            "action": "close",
            "trigger": "now",
            "targets": tools
        } );
    }

    return {
        indexOfTool: indexOfTool,
        addToolOverlay: addToolOverlay,
        removeAppletOverlays: removeAppletOverlays
    };
} () );