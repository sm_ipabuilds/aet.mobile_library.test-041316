// Generate Toolbar Overlays
/* global SM , config , sepUtils , layouts , childLayout , btnLayout , helpLayout , translate */

var generateOverlays = function () {
    
    var toolbarId = SM.generateUniqueId(),
        toolbarChildId = SM.generateUniqueId(),
        toolbarBkgdId = SM.generateUniqueId(),
        toolbarBtnId = SM.generateUniqueId(),
        helpOverlayId = SM.generateUniqueId(),
        innerHelpContainerId = SM.generateUniqueId(),
        localizedLibraryBtn = translate.forString( "LibraryButton" );

     return {
        innerHelpContainerId: innerHelpContainerId,
     
        toolbar : {
            "overlayId": toolbarId,
            "type": "container",
            "persistence": config.persistence,
            "relative": "screen",
            "backgroundColor": "#000000",
            "backgroundAlpha": 0.85,
            "hidden":true,
            "layouts": layouts.layouts,
            "overlays": [
                {
                    "overlayId": "sepToolbar_ExitButton",
                    "type": "button",
                    "images": [ config.plugin + "/assets/" + localizedLibraryBtn + ".png" ],
                    "imagesDown": [ config.plugin + "/assets/" + localizedLibraryBtn + "-tap.png" ],
                    "relative": "parent",
                    "layouts": {
                        "landscape": {
                            "horizontalAlign": "center",
                            "verticalAlign": "top",
                            "x": "50%",
                            "y": "15px"
                        },
                        "portrait": {
                            "horizontalAlign":"center",
                            "verticalAlign":"top",
                            "x": "50%",
                            "y":"15px"
                        }
                    },
                    "actions": [
                        {
                            "action": "exitaction",
                            "trigger": "touchUpInside",
                            "target": config.instanceId
                        }
                    ]
                }
            ],
            "actions": [
                {
                    "action": "hideToolbarAction",
                    "trigger": "swipe-left",
                    "target": config.instanceId
                }
            ]
        },

        toolbarChild : {
            "overlayId": toolbarChildId,
            "type": "container",
            "relative": "parent",
            "layouts": childLayout.layouts,
            "layoutType": "flow",
            "layoutOptions": childLayout.layoutOptions,
            "overlays": []
        },
        
        toolbarBkgd : {
            "overlayId": toolbarBkgdId,
            "type": "container",
            "persistence": config.persistence,
            "relative": "screen",
            "hidden":true,
            "layouts": layouts.makeLayouts( {
                "verticalAlign": "center",
                "horizontalAlign": "center",
                "x": "50%",
                "y": "50%",
                "width": "100%",
                "height": "100%"
            } ).build(),
            "backgroundColor": "#000000",
            "backgroundAlpha": 0.2,
            "overlays": [
                {
                    "type": "button",
                    "relative": "parent",
                    "layouts": layouts.makeLayouts( {
                        "verticalAlign": "center",
                        "horizontalAlign": "center",
                        "x": "50%",
                        "y": "50%",
                        "width": "100%",
                        "height": "100%"
                    } ).build(),
                    "actions": [
                        {
                            "action": "hideToolbarAction",
                            "trigger": "touchUpInside",
                            "target": config.instanceId
                        }
                    ]
                }
            ]
        },
        
        toolbarBtn: {
            "overlayId": toolbarBtnId,
            "type": "button",
            "persistence": config.persistence,
            "relative": "screen",
            "hidden" : true,
            "layouts": btnLayout.layouts,
            "actions": [
                {
                    "action": "showtoolbaraction",
                    "trigger": "swipe-right",
                    "target": config.instanceId
                },
                {
                    "action": "showtoolbaraction",
                    "trigger": "touchUpInside",
                    "target": config.instanceId
                }
            ]
        },

        helpOverlay: {
            "overlayId": helpOverlayId,
            "type": "container",
            "persistence": config.persistence,
            "relative": "screen",
            "layouts": layouts.makeLayouts( {
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "50%",
                "y": "50%",
                "width": "100%",
                "height": "100%"
            } ).build(),
            "backgroundColor": "#000000",
            "backgroundAlpha": 0,
            "overlays": [
                {
                    "overlayId": innerHelpContainerId,
                    "type": "container",
                    "relative": "parent",
                    "backgroundColor": "#ffffff",
                    "backgroundAlpha": 1,
                    "cornerRadius": "10px",
                    "shadowColor": "#000000",
                    "shadowOffsetHeight": "0px",
                    "shadowOffsetWidth": "0px",
                    "shadowOpacity": "0.2",
                    "shadowRadius": "10px",
                    "layouts": helpLayout.containerLayouts,
                    "overlays": [
                        {
                            "type": "container",
                            "relative": "parent",
                            "backgroundColor": "#ffffff",
                            "backgroundAlpha": 1,
                            "cornerRadius": "10px",
                            "clipToBounds": true,
                            "layouts": helpLayout.containerLayouts,
                            "overlays": [
                                {
                                    "type": "text",
                                    "relative": "parent",
                                    "layouts": helpLayout.titleTextLayouts,
                                    "font": "AvenirLTStd-Medium",
                                    "size": "1em",
                                    "text": translate.forString( "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Presentation Tools</span><br /><span style='font-size:16px;'>Swipe from the left edge of the screen to draw, share or return to the library.</span></span>" )
                                },
                                {
                                    "type": "image",
                                    "relative": "parent",
                                    "layouts": helpLayout.imgLayouts,
                                    "images": [ "../" + config.properties.defaultPackage + "/" + config.plugin + "/assets/swlipeRightGraphic.png" ]
                                },
                                {
                                    "type": "button",
                                    "relative": "parent",
                                    "layouts": helpLayout.acceptBtn1Layouts,
                                    "cgBorderAlpha": 0,
                                    "cgBorderColor": "#7fccdd",
                                    "cgBorderWidth": "1px",
                                    "cgButtonAlpha": 0,
                                    "cgButtonPressedAlpha": 0,
                                    "cgButtonPressedColor": "#c3daee",
                                    "cgButtonShineEffect": false,
                                    "cgCornerRadius": "0px",
                                    "font": "AvenirLTStd-Medium",
                                    "fontColor": "#0099bb",
                                    "fontPressedColor": "#c3daee",
                                    "textAlign": "center",
                                    "borderWidth": "1px",
                                    "borderColor": "#cdcdcd",
                                    "size": "1.5em",
                                    "text": translate.forString( "Don't show again" ),
                                    "actions": [
                                        {
                                            "action": "runScriptAction",
                                            "trigger": "touchUpInside",
                                            "target": config.instanceId,
                                            "data": {
                                                "script": "userSettings.setSetting( 'sep_toolbar_was_displayed' , 'true' );"
                                            }
                                        },
                                         {
                                            "action": "closeHelp",
                                            "trigger": "touchUpInside",
                                            "target": config.instanceId
                                         }
                                    ]
                                },
                                 {
                                    "type": "button",
                                    "relative": "parent",
                                    "layouts": helpLayout.acceptBtn2Layouts,
                                    "cgBorderAlpha": 0,
                                    "cgBorderColor": "#7fccdd",
                                    "cgBorderWidth": "0px",
                                    "cgButtonAlpha": 0,
                                    "cgButtonPressedAlpha": 0,
                                    "cgButtonPressedColor": "#c3daee",
                                    "cgButtonShineEffect": false,
                                    "cgCornerRadius": "0px",
                                    "font": "AvenirLTStd-Medium",
                                    "fontColor": "#0099bb",
                                    "fontPressedColor": "#c3daee",
                                    "textAlign": "center",
                                    "borderWidth": "0px",
                                    "borderColor": "#cdcdcd",
                                    "size": "1.5em",
                                    "text": translate.forString( "OK" ),
                                    "actions": [
                                         {
                                            "action": "closeHelp",
                                            "trigger": "touchUpInside",
                                            "target": config.instanceId
                                         }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    };
};