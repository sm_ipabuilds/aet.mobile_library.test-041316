// defaults.js
/* global SM, config */

( function( global ) {
    "use strict";
	var parameters = SM.getArguments();
    global[ parameters.instanceId ] = {};

    var launchBtnId = SM.generateUniqueId(),
        actions = {},
        pdfRenderStartId = SM.generateUniqueId(),
        pdfRenderFinishedId = SM.generateUniqueId(),
        spinnerId = SM.generateUniqueId(),
        defaultPackage = ( config.properties.presentationToolbarId ) ? "../" + SM.getActivePackageId() + "/" : "";

    var launchBtn = {
        "overlayId": launchBtnId,
        "instanceId": SM.generateUniqueId(),
        "type": "button",
        "relative": config.relative,
        "layouts": config.layouts,
        "images": [ config.properties.image ],
        "imagesDown": [ config.properties.imageDown ],
        "actions": [
            {
                "action": "telestrateAction",
                "trigger": "touchUpInside",
                "target": config.instanceId,
                "data": {}
            }
        ]
    };

    function share() {
        SM.runAction( {
            "action":"exportPageAsPDFAction",
            "trigger":"now",
            "source": "#systemPage",
            "target":"#systemPrintManager",
            "data": {
                "exportMethod": "email",
                "filename": "#(pageId).pdf"
            }
        } );
    }

    function initTool() {
        actions[ pdfRenderStartId ] = {
            "action": "#spawn",
            "trigger": "now",
            "data": {
                "overlayId": spinnerId,
                "relative": "screen",
                "persistence": "package",
                "type": "spinner",
                "x": "50%",
                "y": "48%",
                "width": "70px",
                "height": "70px",
                "spinnerCornerRadius": "10px",
                "spinnerStyle": "smallWhite"
            }
        };
        actions[ pdfRenderFinishedId ] = {
            "action": "close",
            "trigger": "now",
            "target": spinnerId
        };

        SM.addContentSpecModule( { "actions": actions } , config.instanceId );
    }

    initTool();
    //-- Actions --//
    global[ parameters.instanceId ].handleAction = function( action , data ) {
        if( action.toLowerCase() === "enabletoolaction" ) {
            // enableToolAction required for presentation toolbar
            share();
            SM.runAction( {
                "action": "hideToolbarAction",
                "trigger": "now",
                "target": config.properties.presentationToolbarId
            } );
        } else if( action.toLowerCase() === "finishloadingaction" ) {
            SM.runAction( {
                "action": "subscribe",
                "trigger": "now",
                "target": "#eventManager",
                "data": {
                    "event": "pdfRenderFinished",
                    "actionId": pdfRenderFinishedId
                }
            } );
            SM.runAction( {
                "action": "subscribe",
                "trigger": "now",
                "target": "#eventManager",
                "data": {
                    "event": "pdfRenderStart",
                    "actionId": pdfRenderStartId
                }
            } );
        } else if( action.toLowerCase() === "updateforlayoutchange" ) {}

     };

    global[ parameters.instanceId ].getOutput = function( name ) {
        if( name.toLowerCase() == "myOutput" ) {
            return "My Output";
        }
    };
    
    //The close method is called when a plugin overlay is closed.
    global[ parameters.instanceId ].close = function() {
        //Remove the plugin spec from the main content.
        // SM.removeContentSpecModule( parameters.instanceId );
    };

    // Tool should be added to presentation toolbar
    if( config.properties.presentationToolbarId ) {
        // Tool is not in the presentation toolbar
        if( !config.properties.isInToolbox ) {
            // Close the original plugin overlay
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "target": parameters.instanceId
            } );

            // Add tool to presentation toolbar
            SM.runAction( {
                "action": "addToolToBoxAction",
                "trigger": "now",
                "target": config.properties.presentationToolbarId,
                "data": {
                    "title": "Share",
                    "tool": config
                }
            } );
            
        }
    } else {
        SM.spawn( launchBtn );
    }
 } ( this ) );