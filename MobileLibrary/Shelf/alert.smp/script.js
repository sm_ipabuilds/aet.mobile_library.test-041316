// defaults.js, events.js, animate.js, layouts.js
/* global SM, config */

( function( global ) {
    "use strict";
    var parameters = SM.getArguments();
    global[ parameters.instanceId ] = {};
    var persistence = parameters.persistence;

    var backgroundContainerId = SM.generateUniqueId(),
        messageContainerId = SM.generateUniqueId(),
        passiveMessageContainerId = SM.generateUniqueId(),

        alert_animate_scaleIn = SM.generateUniqueId(),
        alert_animate_alphaIn = SM.generateUniqueId(),
        alert_animate_scaleOut = SM.generateUniqueId(),
        alert_animate_alphaOut = SM.generateUniqueId(),

        imageSize = 58,
        h_padding = 16,
        v_margin = 20;
    
    var alertVisible = false;
    
    // var textFontSize = parseFloat( config.properties.title.size ) / SM.ContentSpec.getContentSpec().screenSupport.screens[ 0 ].fonts[ 0 ].fontSize;

    function naiveClone( obj ) {
        return JSON.parse( JSON.stringify( obj ) );
    }

    function getScreenSize() {
        var size = SM.getScreenSize(),
            orientation = SM.getOrientation();

        // iOS 8 SDK changes the behavior of SM.getScreenSize
        // Normalize SM.getScreenSize
        if( orientation === "landscape" ) {
            // If iOS 8 SDK, width will be greater than height, else return size
            if( size.width > size.height ) {
                size = { width: size.height , height: size.width };
            }
        }

        // return width and height relative to device orientation for SM.moveOverlay
        return ( orientation === "landscape" ) ? { width: size.height , height: size.width } : size;
    }

    var animations = {};
    animations[ alert_animate_scaleIn ] = {
        "type": "Animation",
        "properties": [
            {
                "property": "scale",
                "fromValue": 0.9,
                "toValue": 1,
                "duration": 0.15,
                "animationFunction": "EaseIn"
            }
        ]
    };
    animations[ alert_animate_alphaIn ] = {
        "type": "Animation",
        "properties": [
            {
                "property": "alpha",
                "fromValue": 0,
                "toValue": 1,
                "duration": 0.15,
                "animationFunction": "EaseIn"
            }
        ]
    };
    animations[ alert_animate_scaleOut ] = {
        "type": "Animation",
        "properties": [
            {
                "property": "scale",
                "fromValue": 1,
                "toValue": 0.9,
                "duration": 0.15,
                "animationFunction": "EaseOut"
            }
        ]
    };
    animations[ alert_animate_alphaOut ] = {
        "type": "Animation",
        "properties": [
            {
                "property": "alpha",
                "fromValue": 1,
                "toValue": 0,
                "duration": 0.15,
                "animationFunction": "EaseOut"
            }
        ]
    };


    var alertOverlays = {
        "bkgd": {
            "overlayId": backgroundContainerId,
            "type": "container",
            "persistence": persistence,
            "relative": "screen",
            "x": "50%",
            "y": "50%",
            "width": "100%",
            "height": "100%",
            "backgroundColor": "#000000",
            "backgroundAlpha": 0.65,
            "overlays": [
                {
                    "type": "button",
                    "relative": "parent",
                    "x": "50%",
                    "y": "50%",
                    "width": "100%",
                    "height": "100%",
                    "actions": []
                }
            ]
        },

        "message_container": {
            "overlayId": messageContainerId,
            "type": "container",
            "persistence": persistence,
            "relative": "screen",
            "layouts": config.layouts,
            "layoutType": "flow",
            "layoutOptions": {
                "marginY": "0px",
                "paddingY": "0px",
                "margin-top": "0px",
                "margin-bottom": "0px",
                "margin-left": "0px",
                "margin-right": "0px"
            },
            "backgroundColor": config.backgroundColor,
            "backgroundAlpha": config.backgroundAlpha,
            "cornerRadius": config.cornerRadius,
            "overlays": []
        },
        "message_container_child": {
            "type": "container",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.landscape.width ) - h_padding * 2 + "px",
                    "height": "auto"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.portrait.width ) - h_padding * 2 + "px",
                    "height": "auto"
                }
            },
            "layoutType": "flow",
            "layoutOptions": {
                "marginY": "0px",
                "marginX": "0px",
                "paddingY": "10px",
                "margin-top": v_margin + "px",
                "margin-bottom": v_margin + "px",

                "//comment": "Applying padding to child is intentional",
                "margin-left": h_padding + "px",
                "margin-right": h_padding + "px"
            },
            "overlays": []
        },
        "message_textTitle": {
            "overlayId": SM.generateUniqueId(),
            "type": "text",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.landscape.width ) - h_padding * 2 + "px",
                    "height": "auto"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.portrait.width ) - h_padding * 2 + "px",
                    "height": "auto"
                }
            },
            "multiline": true,
            "fontSize": config.properties.title.size,
            "padding": [ "0px" , "0px" , "0px" , "0px" ],
            "textAlign": "center",
            "font": config.properties.title.font,
            "fontColor": config.properties.title.color,
            "text": ""
        },
        "message_textBody": {
            "overlayId": SM.generateUniqueId(),
            "type": "text",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.landscape.width ) - h_padding * 2 + "px",
                    "height": "auto"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.portrait.width ) - h_padding * 2 + "px",
                    "height": "auto"
                }
            },
            "multiline": true,
            "fontSize": config.properties.body.size,
            "padding": [ "0px" , "0px" , "0px" , "0px" ],
            "textAlign": "center",
            "font": config.properties.body.font,
            "fontColor": config.properties.body.color,
            "text": ""
        },

        "message_container_img_child": {
            "type": "container",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": imageSize + h_padding + "px",
                    "height": "auto"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": imageSize + h_padding + "px",
                    "height": "auto"
                }
            },
            "layoutType": "flow",
            "layoutOptions": {
                "marginY": "0px",
                "marginX": "0px",
                "paddingY": "0px",
                "margin-top": v_margin + "px",
                "margin-bottom": "0px",

                "//comment": "Applying padding to child is intentional",
                "margin-left": h_padding + "px",
                "margin-right": "0px"
            },
            "overlays": []
        },
        "message_img": {
            "type": "image",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": imageSize + "px",
                    "height": imageSize + "px"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": imageSize + "px",
                    "height": imageSize + "px"
                }
            },
            "scaleMode": "aspect-fit",
            "images": [ "" ]
        },
        
        "v_line": {
            "overlayId": SM.generateUniqueId(),
            "type": "container",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": "1px",
                    "height": "100%"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": "1px",
                    "height": "100%"
                }
            },
            "backgroundColor": "#979797",
            "backgroundAlpha": 0.5
        },
        "h_line": {
            "overlayId": SM.generateUniqueId(),
            "type": "container",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": "100%",
                    "height": "1px"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": "100%",
                    "height": "1px"
                }
            },
            "backgroundColor": "#979797",
            "backgroundAlpha": 0.5
        },

        "message_button_container": {
            "overlayId": SM.generateUniqueId(),
            "type": "container",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.landscape.width ) + "px",
                    "height": "48px"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.portrait.width ) + "px",
                    "height": "48px"
                }
            },
            "layoutType": "flow",
            "layoutOptions": {
                "marginY": "0px",
                "paddingY": "0px"
            },
            "overlays": []
        },
        "message_button": {
            "overlayId": SM.generateUniqueId(),
            "type": "button",
            "relative": "parent",
            "layouts": {
                "landscape": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.landscape.width ) + "px",
                    "height": "48px"
                },
                "portrait": {
                    "horizontalAlign": "center",
                    "verticalAlign": "center",
                    "width": parseFloat( config.layouts.portrait.width ) + "px",
                    "height": "48px"
                }
            },
            "cgBorderAlpha": 0,
            "cgBorderColor": "#979797",
            "cgBorderWidth": "1px",
            "cgButtonAlpha": 0,
            "cgButtonColor": "#FFFFFF",
            "cgButtonPressedAlpha": 0,
            "cgButtonPressedColor": "#FFFFFF",
            "cgButtonShineEffect": false,
            "cgCornerRadius": "0px",
            "font": config.properties.button.font,
            "fontSize": config.properties.button.size,
            "fontColor": config.properties.button.color,
            "fontPressedColor": config.properties.button.pressedColor,
            "textAlign":"center",
            "fixedSize": true,
            "text": ""
        }
    };

    /****************************************************************/
    /*********************** Alert Pop up ***************************/
    /****************************************************************/

    function makeRoomForImage( data ) {
        data.message_container_img_child.overlays = [ data.message_img ];
        data.message_container_child.layouts.landscape.width = parseFloat( data.message_container_child.layouts.landscape.width ) - parseFloat( data.message_container_img_child.layouts.landscape.width ) + "px";
        data.message_container_child.layouts.portrait.width = parseFloat( data.message_container_child.layouts.portrait.width ) - parseFloat( data.message_container_img_child.layouts.portrait.width ) + "px";

        data.message_textTitle.layouts.landscape.width = parseFloat( data.message_textTitle.layouts.landscape.width ) - parseFloat( data.message_container_img_child.layouts.landscape.width ) + "px";
        data.message_textTitle.layouts.portrait.width = parseFloat( data.message_textTitle.layouts.portrait.width ) - parseFloat( data.message_container_img_child.layouts.portrait.width ) + "px";
        data.message_textBody.layouts.landscape.width = parseFloat( data.message_textBody.layouts.landscape.width ) - parseFloat( data.message_container_img_child.layouts.landscape.width ) + "px";
        data.message_textBody.layouts.portrait.width = parseFloat( data.message_textBody.layouts.portrait.width ) - parseFloat( data.message_container_img_child.layouts.portrait.width ) + "px";
    }
    function createAlert( titleText , bodyText , buttonArray , image ) {
        var local = naiveClone( alertOverlays ),
            i = 0,
            len = buttonArray.length,
            v_lines = len - 1,
            temp,
            v_line,
            hasTitle = ( titleText.length > 0 ) ? true : false,
            hasBody = ( bodyText.length > 0 ) ? true : false,
            hasImage = ( image ) ? true : false,
            buttonOverlays = [];
        local.message_textTitle.text = applyStyle( titleText , config.properties.title.fontSuffix );
        local.message_textBody.text = applyStyle( bodyText , config.properties.body.fontSuffix );

        for( i ; i < len ; i++ ) {
            temp = naiveClone( local.message_button );
            temp.text = buttonArray[ i ].text;
            if( buttonArray[ i ].fontSuffix ) { temp.font += buttonArray[ i ].fontSuffix; }

            temp.layouts.landscape.width = ( parseFloat( config.layouts.landscape.width ) - v_lines ) / len + "px";
            temp.layouts.portrait.width = ( parseFloat( config.layouts.portrait.width ) - v_lines ) / len + "px";
            temp.actions = buttonArray[ i ].actions;
            v_line = naiveClone( local.v_line );
            v_line.index = i; // objects must be unique, why?? Could it be the container type?
            buttonOverlays.push( temp );
            if( i < v_lines && len !== 1 ) { buttonOverlays.push( v_line ); }
        }

        if( hasImage ) {
            local.message_img.images = [ image ];
            makeRoomForImage( local );
        }

        local.message_button_container.overlays = buttonOverlays;
        local.message_container_child.overlays = [];
        if( hasTitle ) { local.message_container_child.overlays.push( local.message_textTitle ); }
        if( hasBody ) { local.message_container_child.overlays.push( local.message_textBody ); }
        if( hasImage ) {
            local.message_container.overlays = [ local.message_container_img_child , local.message_container_child , local.h_line , local.message_button_container ];
        } else {
            local.message_container.overlays = [ local.message_container_child , local.h_line , local.message_button_container ];
        }

        return local;
    }
    function spawnAlert( titleText , bodyText , buttonArray , image ) {
        var alert = createAlert( titleText , bodyText , buttonArray , image ),
            screenSize = getScreenSize();

        SM.spawn( alert.bkgd );
        SM.spawn( alert.message_container );
        
        //Animations break while modal pageset is presented
        if(SM.getProperty("#systemPage", "isPresentingModal") == "false"){
        
			SM.runAction( {
				"action": "animate",
				"trigger": "now",
				"targets": [ backgroundContainerId , messageContainerId ],
				"data": {
					"animationId": alert_animate_alphaIn
				}
			} );
			SM.runAction( {
				"action": "animate",
				"trigger": "now",
				"target": messageContainerId,
				"data": {
					"animationId": alert_animate_scaleIn
				}
			} );        	
        }

        // Auto height containers do not align vertically as expected
        // This is a work around to center alert message
        SM.moveOverlay(alert.message_container.overlayId ,
            Math.floor( parseFloat( screenSize.width / 2 ) ) + "px",
            Math.floor( parseFloat( screenSize.height / 2 ) ) + "px" );
    }
    function applyStyle( text , style ) {
        var string;
        style = style.replace( /\W/gi , "" );
        if( style === "bold" ) {
            string = "<strong><%= text %></strong>";
        } else {
            return text;
        }

        return string.replace(/<%=\s+(.*?)\s+%>/g, function (m, m1) {
            return text;
        });
    }

    var messageOverlays = {
        "message_container": {
            "overlayId": passiveMessageContainerId,
            "type": "container",
            "relative": "screen",
            "horizontalAlign": "center",
            "verticalAlign": "center",
            "x": "50%",
            "y": "40%",
            "width": "200px",
            "height": "auto",
            "layoutType": "flow",
            "layoutOptions": {
                "marginY": "0px",
                "paddingY": "16px",
                "margin-top": "0px",
                "margin-bottom": "0px",
                "margin-left": "0px",
                "margin-right": "0px"
            },
            "overlays": []
        },
        "message_img": {
            "overlayId": SM.generateUniqueId(),
            "type": "image",
            "relative": "parent",
            "horizontalAlign": "center",
            "verticalAlign": "center",
            "width": "100%",
            "scaleMode": "center",
            "images": []
        },
        "message_text": {
            "overlayId": SM.generateUniqueId(),
            "type": "text",
            "relative": "parent",
            "horizontalAlign": "center",
            "verticalAlign": "center",
            "width": "100%",
            "multiline": true,
            "size": "1.2em",
            "font":"AvenirLTStd-Medium",
            "fontColor": "#4D4D4D",
            "textAlign": "center",
            "text": ""
        }
    };
    function spawnPassiveMessage( data ) {
        spawnPassiveMessage.spawned = spawnPassiveMessage.spawned || false;

        var screenSize = getScreenSize(),
            overlays = naiveClone( messageOverlays ),
            xPos = 0.5,
            yPos = 0.35;

        if( !spawnPassiveMessage.spawned ) {
            overlays.message_img.images = [ data.image ];
            
            overlays.message_text.text = data.text;
            overlays.message_text.fontColor = data.fontColor || "#4D4D4D";
            overlays.message_text.size = data.size || config.properties.title.size || "1em";
            
            overlays.message_container.overlays = [ overlays.message_img , overlays.message_text ];
            SM.spawn( overlays.message_container );
            spawnPassiveMessage.spawned = true;
            spawnPassiveMessage.messageOverlayId = overlays.message_container.overlayId;

            // Auto height containers do not align vertically as expected
            // This is a work around to center alert message
            SM.moveOverlay(overlays.message_container.overlayId ,
                Math.floor( parseFloat( screenSize.width * xPos ) ) + "px",
                Math.floor( parseFloat( screenSize.height * yPos ) ) + "px" );
        }
    }

    SM.addContentSpecModule( { "animations": animations } , config.instanceId );

    //-- Actions --//
    global[ parameters.instanceId ].handleAction = function( action , data ) {

        if( action.toLowerCase() === "displayalertaction" ) {
            if(!alertVisible){
                spawnAlert( data.title , data.body , data.buttons , data.image || "" );
                alertVisible = true;
            }

        } else if( action.toLowerCase() === "dismissalertaction" ) {
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "targets": [ backgroundContainerId , messageContainerId ],
                "data": {
                    "animationId": alert_animate_alphaOut
                }
            } );
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": messageContainerId,
                "data": {
                    "animationId": alert_animate_scaleOut
                }
            } );
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "delay": 0.15,
                "targets": [ backgroundContainerId , messageContainerId ]
            } );
            alertVisible = false;
        } else if( action.toLowerCase() === "displaypassivemessageaction" ) {
            if( spawnPassiveMessage.spawned ) {
                global[ parameters.instanceId ].handleAction( "dismissPassiveMessageAction" , {} );    
            }
            spawnPassiveMessage( data );
        } else if( action.toLowerCase() === "dismisspassivemessageaction" ) {
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "target": passiveMessageContainerId
            } );
            spawnPassiveMessage.spawned = false;
            spawnPassiveMessage.messageOverlayId = null;
        } else if( action.toLowerCase() === "updateforlayoutchange" ) {
			var screenSize = getScreenSize();        
			
            if(spawnPassiveMessage.messageOverlayId){            
                    var xPos = 0.5,
                    yPos = 0.35;            
                SM.moveOverlay(spawnPassiveMessage.messageOverlayId,
                    Math.floor( parseFloat( screenSize.width * xPos ) ) + "px",
                    Math.floor( parseFloat( screenSize.height * yPos ) ) + "px" );            
            }
			SM.moveOverlay(messageContainerId,
				Math.floor( parseFloat( screenSize.width / 2 ) ) + "px",
				Math.floor( parseFloat( screenSize.height / 2 ) ) + "px" );            
        }
     };

    global[ parameters.instanceId ].getOutput = function( name ) {
        if( name.toLowerCase() == "myOutput" ) {
            return "My Output";
        }
    };

    global[ parameters.instanceId ].close = function() {
        SM.runAction( {
            "action": "close",
            "trigger": "now",
            "targets": [ backgroundContainerId , config.instanceId ]

        } );
    };
 } ( this ) );