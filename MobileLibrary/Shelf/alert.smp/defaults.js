// Default Plugin parameters
/* global SM */

var config = ( function( params ) {
    "use strict";
    function isObject( obj ) {
        return ( typeof obj === "object" ) ? true : false;
    }
    function duplicateJSON( object ) {
        var dup = ( object instanceof Array ) ? [] : {},
            key;
        for( key in object ) {
            if ( object[ key ] instanceof Array || object[ key ] instanceof Object ) {
                dup[ key ] = duplicateJSON( object[ key ] );
            } else {
                dup[ key ] = object[ key ];
            }
        }
        return dup;
    }
    function mergeConfig( defaultConfig , userConfig ) {
        var merged = duplicateJSON( defaultConfig );
        var key;

        for( key in userConfig ) {
            if( !isObject( userConfig[ key ] ) ) {
                merged[ key ] = userConfig[ key ];
            } else {
                merged[ key ] = mergeConfig( merged[ key ] , userConfig[ key ] );
            }
        }
        return merged;
    }

    var defaults = {
        "overlayId": SM.generateUniqueId(),
        "instanceId": "",
        "relative": "screen",
        "layouts": {
            "landscape": {
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "50%",
                "y": "50%",
                "width": "300px",
                "height": "auto"
            },
            "portrait": {
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "50%",
                "y": "50%",
                "width": "300px",
                "height": "auto"
            }
        },
        "backgroundColor": "#FFFFFF",
        "backgroundAlpha": 0.95,
        "cornerRadius": "0px",
        "properties": {
            "title": {
                "size": "1.6em",
                "fontSuffix": "-bold",
                "font": "Helvetica",
                "color": "#000000"
            },
            "body": {
                "size": "1.6em",
                "fontSuffix": "-regular",
                "font": "Helvetica",
                "color": "#000000"
            },
            "button": {
                "size": "1.6em",
                "font": "Helvetica",
                "color": "#0082CA",
                "pressedColor": "#c3DAEE"
            }
            
        }
    };
    return mergeConfig( defaults , params );
} ( SM.getArguments() ) );