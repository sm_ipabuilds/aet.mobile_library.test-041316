describe( "ScrollMotion JavaScript Bridge, \"SM\"" , function() {
	it( "is an Object" , function() {
		expect( SM ).toEqual( jasmine.any( Object ) );
	} );
	it( "logs messages to the console, \"SM.log\"" , function() {
		expect( SM.log ).toEqual( jasmine.any( Function ) );
	} );
	it( "alerts messages to the app, \"SM.alert\"" , function() {
		expect( SM.alert ).toEqual( jasmine.any( Function ) );
	} );
	it( "gets the device orientation, \"SM.getOrientation\"" , function() {
		expect( SM.getOrientation ).toEqual( jasmine.any( Function ) );
	} );
	it( "gets the device screen size, \"SM.getScreenSize\"" , function() {
		expect( SM.getScreenSize ).toEqual( jasmine.any( Function ) );
	} );
	it( "gets overlay property outputs, \"SM.getProperty\"" , function() {
		expect( SM.getProperty ).toEqual( jasmine.any( Function ) );
	} );
	it( "moves an overlay, \"SM.moveOverlay\"" , function() {
		expect( SM.moveOverlay ).toEqual( jasmine.any( Function ) );
	} );
	it( "hides an overlay, \"SM.setHidden\"" , function() {
		expect( SM.setHidden ).toEqual( jasmine.any( Function ) );
	} );

	describe( "Run Action method, \"SM.runAction\"" , function() {
		beforeEach( function() {
			spyOn( SM , "runAction" );
			SM.runAction( {} );
		} );

		it( "calls Content Spec Actions" , function() {
			expect( SM.runAction ).toHaveBeenCalled();
		} );
		it( "accepts a single Object parameter" , function() {
			expect( SM.runAction ).toHaveBeenCalledWith( {} );
		} );
	} );
	describe( "Set Text method, \"SM.setText\"" , function() {
		beforeEach( function() {
			spyOn( SM , "setText" );
			SM.setText( "overlayId" , "textString" );
		} );

		it( "call setTextAction Action" , function() {
			expect( SM.setText ).toHaveBeenCalled();
		} );
		it( "accepts two String parameters" , function() {
			expect( SM.setText.calls.mostRecent() ).toEqual( { object: SM , args: [ "overlayId" , "textString" ] } );
		} );
	} );
} );