/* global jasmine , describe , xdescribe , it , xit , expect , spyOn , beforeEach , afterEach , pending ,
SM , userSettings , sepEulaAgreement , SEP */

describe( "Utility functions, \"ISDEBUG\"" , function() {
    "use strict";
    describe( "with a debug flag, \"userSettings\"" , function() {
        it( "to not be undefined" , function() {
            expect( ISDEBUG ).not.toBe( undefined );
        } );
    } );

    /*
        * @requires saveUserSettingAction
    */
    describe( "User Settings, \"userSettings\"" , function() {
        beforeEach( function() {
            spyOn( SM , "runAction" ).and.callThrough();
            spyOn( SM , "getProperty" ).and.callThrough();
        } );
        it( "sets key value pairs based on the user, \"userSettings.setSetting\"" , function() {
            userSettings.setSetting( "testKey" , "testValue" );
            expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                action: "saveUserSettingAction",
                target: "#systemPackageDataRegistry",
                data: {
                    settingKey: "testKey",
                    settingValue: "testValue"
                }
            } ) );
        } );
        it( "gets key value based on the user" , function() {
            userSettings.setSetting( "testKey" , "testValue" );
            var value = userSettings.getSetting( "testKey" );
            expect( value ).toEqual( jasmine.any( String ) );
            expect( SM.getProperty ).toHaveBeenCalledWith( "#systemPackageDataRegistry" , "userSettings.testKey" );
        } );
    } );

    describe( "SEP legal terms, \"sepEulaAgreement\"" , function() {
        /* 
        TO DO:
            1. Add unit test for legal terms version number
        */

        beforeEach( function() {
            spyOn( SM , "runAction" ).and.callThrough();
            spyOn( userSettings , "setSetting" ).and.callThrough();
            spyOn( userSettings , "getSetting" ).and.callThrough();
            spyOn( sepEulaAgreement , "bypassEULA" ).and.callThrough();
        } );
        afterEach( function() {
            // Clear session storage and saved user data for testing
            if(typeof(Storage) !== "undefined") {
                for( var key in sessionStorage ) {
                    sessionStorage.removeItem( key );
                }
            }
        } );

        it( "displays OS requirements if needed, \"sepEulaAgreement.upgradeIOS\"" , function() {
            var target = "updateRequired";

            sepEulaAgreement.upgradeIOS( false , target );
            expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage"
            } ) );

            sepEulaAgreement.upgradeIOS( true , target );
            expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage",
                "data":  jasmine.objectContaining( {
                    "pageId": target
                } )
            } ) );
        } );
        it( "will bypass the legal terms under right circumstances, \"sepEulaAgreement.bypassEULA\"" , function() {
            var firstUser = "legalTerms",
                accepted = "getInThere";

            sepEulaAgreement.bypassEULA( false , firstUser , accepted );
            expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage",
                "data": jasmine.objectContaining( {
                    "pageId": firstUser
                } )
            } ) );

            sepEulaAgreement.bypassEULA( true , firstUser , accepted );
            expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage",
                "data": jasmine.objectContaining( {
                    "pageId": accepted
                } )
            } ) );
        } );
        it( "saves user legal terms acceptance, \"sepEulaAgreement.acceptEULA\"" , function() {
            var firstUser = "legalTerms",
                accepted = "getInThere";

            sepEulaAgreement.acceptEULA( accepted , "crossfade" );
            expect( userSettings.setSetting ).toHaveBeenCalled();
            expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                "action": "gotoPageAction",
                "trigger": "now",
                "target": "#systemPage",
                "data": jasmine.objectContaining( {
                    "pageId": accepted
                } )
            } ) );
        } );
            
        describe( "Legal terms, \"sepEulaAgreement.showEulaOnce\"" , function() {
            var firstUser = "legalTerms",
                accepted = "getInThere";
            it( "will display page target for first users if failed to accept terms" , function() {
                sepEulaAgreement.showEulaOnce( firstUser , accepted );
                expect( sepEulaAgreement.bypassEULA ).toHaveBeenCalledWith( false , firstUser , accepted );
            } );
            it( "will display accepted target for users that have accepted the terms" , function() {
                sepEulaAgreement.acceptEULA( accepted , "crossfade" );
                
                sepEulaAgreement.showEulaOnce( firstUser , accepted );
                expect( sepEulaAgreement.bypassEULA ).toHaveBeenCalledWith( true , firstUser , accepted );
                expect( userSettings.setSetting ).toHaveBeenCalled();
            } );
        } );
    } );
} );

describe( "SEP suite, \"SEP\"" , function() {
    "use strict";
    it( "is an Object" , function() {
        expect( SEP ).toEqual( jasmine.any( Object ) );
    } );

    describe( "Events, \"SEP.events\"" , function() {
        var spy = jasmine.createSpy( "spy" ),
            publishSpy = jasmine.createSpy( "spy" ),
            tokens = [];

        beforeEach( function() {
            spyOn( SEP.events , "subscribe" ).and.callThrough();
            spyOn( SEP.events , "publish" ).and.callThrough();
            spyOn( SEP.events , "unsubscribe" ).and.callThrough();
        } );

        it( "is an Object" , function() {
            expect( SEP.events ).toEqual( jasmine.any( Object ) );
        } );
        
        describe( "Subscribe to event, \"SEP.events.subscribe\"" , function() {
            var subscribeToken1 = undefined,
                subscribeToken2 = undefined;

            it( "executed with a event String and a callback function" , function() {
                subscribeToken1 = SEP.events.subscribe( "test" , spy );
                subscribeToken2 = SEP.events.subscribe( "test" + Math.random() , function() {} );

                tokens.push( subscribeToken1 );
                tokens.push( subscribeToken2 );

                expect( SEP.events.subscribe ).toHaveBeenCalled();
                expect( SEP.events.subscribe.calls.argsFor( 0 ) ).toEqual( [ "test" , jasmine.any( Function ) ] );
            } );

            it( "returns a unique String token id" , function() {
                expect( subscribeToken1 ).toEqual( jasmine.any( String ) );
                expect( subscribeToken2 ).not.toEqual( subscribeToken1 );
            } );
        } );

        describe( "Publish event, \"SEP.events.publish\"" , function() {
            it( "triggers event callback with arguments" , function() {
                SEP.events.publish( "test" , { testIsSuccessful: true } );
                expect( spy ).toHaveBeenCalledWith( "test" , jasmine.objectContaining( { testIsSuccessful: true } ) );
            } );
            it( "can be chained with other SEP.events methods" , function() {
                var publishTest,
                    publishToken = SEP.events.subscribe( "publishtest" , publishSpy );

                tokens.push( publishToken );
                
                publishTest = SEP.events.publish( "publishtest" , { testIsSuccessful: true } );
                expect( publishTest ).toEqual( SEP.events );
                expect( publishSpy.calls.count() ).toEqual( 1 );
                expect( publishSpy ).toHaveBeenCalledWith( "publishtest" , jasmine.objectContaining( { testIsSuccessful: true } ) );

                publishTest.publish( "publishtest" , { onceMore: true } );
                expect( publishSpy.calls.count() ).toEqual( 2 );
                expect( publishSpy ).toHaveBeenCalledWith( "publishtest" , jasmine.objectContaining( { onceMore: true } ) );
            } );
            it( "supports a partial application to modify data on the fly" , function() {
                var transformer = SEP.events.publish( function( event , args ) {
                    args.transformed = true;
                } );

                transformer( "publishtest" , { testIsSuccessful: true } );
                expect( SEP.events.publish ).toHaveBeenCalled();
                // requires previous test case for success
                expect( publishSpy ).toHaveBeenCalledWith( "publishtest" , jasmine.objectContaining( { testIsSuccessful: true , transformed: true } ) );
            } );
            it( "will return false when calling an unregistered event" , function() {
                expect( SEP.events.publish( "thisdoesnotexist" , {} ) ).toEqual( false );
            } );

        } );
        describe( "Unsubscribe event, \"SEP.events.unsubscribe\"" , function() {
            it( "revokes Function callback on event triggers" , function() {
                var returnValue;
                for( var i = 0 , len = tokens.length; i < len; i++ ) {
                    returnValue = SEP.events.unsubscribe( tokens[ i ] );

                    // return String token if successful
                    expect( returnValue ).toEqual( jasmine.any( String ) );
                    expect( SEP.events.unsubscribe ).toHaveBeenCalledWith( jasmine.any( String ) );
                }
                expect( SEP.events.unsubscribe.calls.count() ).toEqual( len );

                spy.calls.reset();
                publishSpy.calls.reset();

                SEP.events.publish( "test" , { testIsSuccessful: true } );
                SEP.events.publish( "publishtest" , { testIsSuccessful: true } );
                SEP.events.publish( "publishtest" , { onceMore: true } );

                expect( spy ).not.toHaveBeenCalled();
                expect( publishSpy ).not.toHaveBeenCalled();
            } );
            it( "will return SEP.events when unsubscribing to an invalid token" , function() {
                // Decimal values are not valid
                expect( SEP.events.unsubscribe( "1.01" ) ).toEqual( SEP.events );
            } );

        } );
    } );

    describe( "Network connectivity, \"SEP.network\"" , function() {
        it( "is an Object" , function() {
            expect( SEP.network ).toEqual( jasmine.any( Object ) );
        } );

        it( "will execute a callback when offline, \"SEP.network.callbackOnConnection\"" , function() {
            SEP.network.setConnectivity( false );
            var spy = jasmine.createSpy( "networkSpy" );

            SEP.network.callbackOnConnection( "offline" , spy );
            expect( spy ).toHaveBeenCalled();
        } );
        it( "will execute a callback when online, \"SEP.network.callbackOnConnection\"" , function() {
            SEP.network.setConnectivity( true );
            var spy = jasmine.createSpy( "networkSpy" );

            SEP.network.callbackOnConnection( "online" , spy );
            expect( spy ).toHaveBeenCalled();
        } );
        it( "will execute a callback when online and using WIFI, \"SEP.network.callbackOnConnection\"" , function() {
            SEP.network.setConnectivity( true );
            SEP.network.setConnectionType( "wifi" );
            var spy = jasmine.createSpy( "networkSpy" );

            SEP.network.callbackOnConnection( "wifi" , spy );
            expect( spy ).toHaveBeenCalled();
        } );
        it( "will not execute a callback when offline and using WIFI" , function() {
            SEP.network.setConnectivity( false );
            SEP.network.setConnectionType( "wifi" );
            var spy = jasmine.createSpy( "networkSpy" );

            SEP.network.callbackOnConnection( "wifi" , spy );
            expect( spy ).not.toHaveBeenCalled();
        } );

        it( "will execute a callback when online and using WWAN, \"SEP.network.callbackOnConnection\"" , function() {
            SEP.network.setConnectivity( true );
            SEP.network.setConnectionType( "wwan" );
            var spy = jasmine.createSpy( "networkSpy" );

            SEP.network.callbackOnConnection( "wwan" , spy );
            expect( spy ).toHaveBeenCalled();
        } );
        it( "will not execute a callback when offline and using WWAN" , function() {
            SEP.network.setConnectivity( false );
            SEP.network.setConnectionType( "wwan" );
            var spy = jasmine.createSpy( "networkSpy" );

            SEP.network.callbackOnConnection( "wwan" , spy );
            expect( spy ).not.toHaveBeenCalled();
        } );
    } );

    /*
        * @requires Modified SM.getOrientation, SM.getScreenSize
    */
    describe( "Device type, \"SEP.device\"" , function() {
        /*
            TO DO:
                1. width and height should be assumed to be in portrait orientation regardless of device rotation.
                    Ie., width value will always be less than height value
        */

        it( "is an Object" , function() {
            expect( SEP.device ).toEqual( jasmine.any( Object ) );
        } );

        describe( "iPad" , function() {
            beforeEach( function() {
                // Force iPad device size
                SM.getScreenSize = function() {
                    return { "width": 768 , "height": 1024 };
                };
            } );
            afterEach( function() {
                // Reset function cache
                SEP.device.callbackOnDevice.width = 0;
                SEP.device.callbackOnDevice.height = 0;
            } );
            it( "will execute a callback for device type" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                SEP.device.callbackOnDevice( "ipad" , spy );
                expect( spy ).toHaveBeenCalled();
            } );
            it( "will pass device parameters: orientation, width, and height" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                // landscape is returned
                SM.getOrientation = function() { return "landscape"; };

                SEP.device.callbackOnDevice( "ipad" , spy );
                expect( spy ).toHaveBeenCalledWith( "landscape" , 768 , 1024 );

                // portrait is returned
                SM.getOrientation = function() { return "portrait"; };
                SEP.device.callbackOnDevice( "ipad" , spy );
                expect( spy ).toHaveBeenCalledWith( "portrait" , 768 , 1024 );
            } );
            it( "will not execute a callback for the incorrect device type" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                SEP.device.callbackOnDevice( "iphone" , spy );
                expect( spy ).not.toHaveBeenCalled();
            } );
        } );
        describe( "iPhone" , function() {
            beforeEach( function() {
                // force iPhone 5 device size
                SM.getScreenSize = function() {
                    return { "width": 320 , "height": 568 };
                };
            } );
            afterEach( function() {
                // Reset function cache
                SEP.device.callbackOnDevice.width = 0;
                SEP.device.callbackOnDevice.height = 0;
            } );
            it( "will execute a callback for device type" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                SEP.device.callbackOnDevice( "iphone" , spy );
                expect( spy ).toHaveBeenCalled();
            } );
            it( "will pass device parameters: orientation, width, and height" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                // landscape is returned
                SM.getOrientation = function() { return "landscape"; };

                SEP.device.callbackOnDevice( "iphone" , spy );
                expect( spy ).toHaveBeenCalledWith( "landscape" , 320 , 568 );

                // portrait is returned
                SM.getOrientation = function() { return "portrait"; };
                SEP.device.callbackOnDevice( "iphone" , spy );
                expect( spy ).toHaveBeenCalledWith( "portrait" , 320 , 568 );
            } );
            it( "will pass device parameters for iphone4: orientation, width, and height" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                // force iPhone 4 device size
                SM.getScreenSize = function() { return { "width": 320 , "height": 480 }; };
                // landscape is returned
                SM.getOrientation = function() { return "landscape"; };

                SEP.device.callbackOnDevice( "iphone" , spy );
                expect( spy ).toHaveBeenCalledWith( "landscape" , 320 , 480 );

                // portrait is returned
                SM.getOrientation = function() { return "portrait"; };
                SEP.device.callbackOnDevice( "iphone" , spy );
                expect( spy ).toHaveBeenCalledWith( "portrait" , 320 , 480 );
            } );
            it( "will not execute a callback for the incorrect device type" , function() {
                var spy = jasmine.createSpy( "deviceSpy" );
                SEP.device.callbackOnDevice( "ipad" , spy );
                expect( spy ).not.toHaveBeenCalled();
            } );
        } );
    } );

    describe( "SEP debugging, \"SEP.debug\"" , function() {
        it( "alerts messages to the app" , function() {
            ISDEBUG = true;

            spyOn( SM , "alert");
            SEP.debug( true , "1" , 2 , { "3" : 4 } , [ 5 ] );
            expect( SM.alert ).toHaveBeenCalled();
        } );
        it( "logs messages to the app" , function() {
            ISDEBUG = true;

            spyOn( SM , "log");
            SEP.debug( "1" , 2 , { "3" : 4 } , [ 5 ] );
            expect( SM.log ).toHaveBeenCalled();
        } );
        it( "will not alert without the window.ISDEBUG flag" , function() {
            ISDEBUG = false;

            spyOn( SM , "alert");
            SEP.debug( true , "1" , 2 , { "3" : 4 } , [ 5 ] );
            expect( SM.alert ).not.toHaveBeenCalled();
        } );
        it( "will not log without the window.ISDEBUG flag" , function() {
            ISDEBUG = false;

            spyOn( SM , "log");
            SEP.debug( "1" , 2 , { "3" : 4 } , [ 5 ] );
            expect( SM.log ).not.toHaveBeenCalled();
        } );
    } );
    
    describe( "SEP timing, \"SEP.timer\"" , function() {
        var aa = SEP.timer();

        it( "returns an integer id" , function() {
            expect( aa ).toEqual( jasmine.any( Number ) );
        } );
        it( "simply elapses time" , function() {
            expect( SEP.timer( aa ) ).toBeGreaterThan( 0 );
        } );
    } );

    describe( "Plugin alert functions for convenience" , function() {
        beforeEach( function() {
            function containsStringKeyValuePair( object , key , value ) {
                var instancesFound = 0;
                function recursive( object ) {
                    for( var k in object ) {
                        if( object[ k ] instanceof Object ) {
                            recursive( object[ k ] );
                        } else if( typeof object[ k ] === "string" ) {
                            if( k === key && object[ k ] === value ) {
                                // console.log( k + ": " + object[ k ] );
                                instancesFound++;
                            }
                        }
                    }
                    return instancesFound;
                }
                return recursive( object );
            }

            jasmine.addMatchers( {
                containsActionKeyValue: function() {
                    return {
                        compare: function( actual , key , value ) {
                            var result = { pass: false };
                            // key = "action";

                            if( typeof actual !== "object" ) {
                                result.message = "Expected action to be an Object not a " + typeof actual;
                                result.pass = false;
                                return result;
                            }
                            result.pass = ( containsStringKeyValuePair( actual , key , value ) > 0 ) ? true : false;
                            if( result.pass ) {
                                result.message = "Contains \"" + key + "\" equal to \"" + value + "\"";
                            } else {
                                result.message = "Expected to find \"" + key + "\" equal to \"" + value + "\"";
                            }
                            return result;
                        }
                    };
                }
            } );
        } );

        describe( "\"SEP.alert\"" , function() {
            it( "is an Object" , function() {
                expect( SEP.alert ).toEqual( jasmine.any( Object ) );
            } );
        } );

        describe( "displays alert upon log in with wrong credentials, \"logInWrongCredentials\"" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;

            beforeEach( function() {
                SEP.updateTranslationForString = function( string ) {
                    return string;
                };

                spyOn( SM , "runAction" );
                SEP.alert.logInWrongCredentials();
            } );
            afterEach( function() {
                SEP.updateTranslationForString = tempUpdateTranslationForString;
            } );

            it( "relies upon \"SM.runAction\" method" , function() {
                expect( SM.runAction ).toHaveBeenCalled();
            } );
            it( "targets the \"alert_plugin\" overlay id and execute \"displayAlertAction\" immediately" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        action: "displayAlertAction",
                        target: "alert_plugin",
                        trigger: "now"
                    } )
                );
            } );
            it( "should allow a user to dismiss the alert, \"dismissAlertAction\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "displayAlertAction" );
            } );
        } );
        describe( "displays alert upon invalid WorkCloud certificate, \"invalidCertificate\"" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;

            beforeEach( function() {
                SEP.updateTranslationForString = function( string ) {
                    return string;
                };

                spyOn( SM , "runAction" );
                SEP.alert.invalidCertificate();
            } );
            afterEach( function() {
                SEP.updateTranslationForString = tempUpdateTranslationForString;
            } );

            it( "relies upon \"SM.runAction\" method" , function() {
                expect( SM.runAction ).toHaveBeenCalled();
            } );
            it( "targets the \"alert_plugin\" overlay id and execute \"displayAlertAction\" immediately" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        action: "displayAlertAction",
                        target: "alert_plugin",
                        trigger: "now"
                    } )
                );
            } );
            it( "should allow a user to dismiss the alert, \"dismissAlertAction\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "displayAlertAction" );
            } );
        } );
        describe( "displays alert upon unresolved host connection, \"hostConnectionError\"" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;

            beforeEach( function() {
                SEP.updateTranslationForString = function( string ) {
                    return string;
                };

                spyOn( SM , "runAction" );
                SEP.alert.hostConnectionError();
            } );
            afterEach( function() {
                SEP.updateTranslationForString = tempUpdateTranslationForString;
            } );

            it( "relies upon \"SM.runAction\" method" , function() {
                expect( SM.runAction ).toHaveBeenCalled();
            } );
            it( "targets the \"alert_plugin\" overlay id and execute \"displayAlertAction\" immediately" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        action: "displayAlertAction",
                        target: "alert_plugin",
                        trigger: "now"
                    } )
                );
            } );
            it( "should allow a user to dismiss the alert, \"dismissAlertAction\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "displayAlertAction" );
            } );
        } );
        describe( "displays alert upon empty shelf view, \"noEntitledPackages\"" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;

            beforeEach( function() {
                SEP.updateTranslationForString = function( string ) {
                    return string;
                };

                spyOn( SM , "runAction" );
                SEP.alert.noEntitledPackages();
            } );
            afterEach( function() {
                SEP.updateTranslationForString = tempUpdateTranslationForString;
            } );

            it( "relies upon \"SM.runAction\" method" , function() {
                expect( SM.runAction ).toHaveBeenCalled();
            } );
            it( "targets the \"alert_plugin\" overlay id and execute \"displayAlertAction\" immediately" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        action: "displayAlertAction",
                        target: "alert_plugin",
                        trigger: "now"
                    } )
                );
            } );
            it( "should allow a user to dismiss the alert, \"dismissAlertAction\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "displayAlertAction" );
            } );
        } );
        describe( "displays alert content with force download and install sync policy, \"forceUpdate\"" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;

            beforeEach( function() {
                SEP.updateTranslationForString = function( string ) {
                    return string;
                };

                spyOn( SM , "runAction" );
                SEP.alert.forceUpdate();
            } );
            afterEach( function() {
                SEP.updateTranslationForString = tempUpdateTranslationForString;
            } );

            it( "relies upon \"SM.runAction\" method" , function() {
                expect( SM.runAction ).toHaveBeenCalled();
            } );
            it( "targets the \"alert_plugin\" overlay id and execute \"displayAlertAction\" immediately" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        action: "displayAlertAction",
                        target: "alert_plugin",
                        trigger: "now"
                    } )
                );
            } );
            it( "requires the \"systemPackageDataRegistry\" source" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        source: "#systemPackageDataRegistry"
                    } )
                );
            } );
            it( "should allow a user to dismiss the alert, \"dismissAlertAction\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "displayAlertAction" );
            } );
        } );
        describe( "displays alert content with force download and optional install sync policy, \"updateAvailable\"" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;

            beforeEach( function() {
                SEP.updateTranslationForString = function( string ) {
                    return string;
                };

                spyOn( SM , "runAction" );
                SEP.alert.updateAvailable();
            } );
            afterEach( function() {
                SEP.updateTranslationForString = tempUpdateTranslationForString;
            } );

            it( "relies upon \"SM.runAction\" method" , function() {
                expect( SM.runAction ).toHaveBeenCalled();
            } );
            it( "targets the \"alert_plugin\" overlay id and execute \"displayAlertAction\" immediately" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        action: "displayAlertAction",
                        target: "alert_plugin",
                        trigger: "now"
                    } )
                );
            } );
            it( "requires the \"systemPackageDataRegistry\" source" , function() {
                expect( SM.runAction ).toHaveBeenCalledWith(
                    jasmine.objectContaining( {
                        source: "#systemPackageDataRegistry"
                    } )
                );
            } );
            it( "should allow a user to install the updated package, \"installPackage\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "installPackage" );
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "source" , "#systemPackageDataRegistry" );
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "target" , "#systemPackageDataRegistry" );
            } );
            it( "should allow a user to dismiss the alert, \"dismissAlertAction\"" , function() {
                expect( SM.runAction.calls.mostRecent() ).containsActionKeyValue( "action" , "displayAlertAction" );
            } );
        } );
        describe( "displays alert upon device network offline, \"deviceOffline\"" , function() {
            pending();
        } );
    } );
    
    describe( "Register Collectionview, \"SEP.RegisterCollectionView\"" , function() {
        var collection,
            collectionOverlayId = "Cloud";
        describe( "initialization" , function() {
            beforeEach( function() {
                spyOn( SEP.events , "subscribe" ).and.callThrough();
                spyOn( userSettings , "getSetting" ).and.callThrough();

                collection = new SEP.RegisterCollectionView( collectionOverlayId , { viewType: "allFiles" } );
            } );
            afterEach( function() {
                collection.clear();
            } );
            
            it( "will register events using SEP.events.subscribe" , function() {
                // load persistent data
                expect( userSettings.getSetting.calls.count() ).toEqual( 3 );

                expect( userSettings.getSetting ).toHaveBeenCalledWith( "sep_history_array" );
                expect( userSettings.getSetting ).toHaveBeenCalledWith( "sep_view_history_array" );
                expect( userSettings.getSetting ).toHaveBeenCalledWith( "sep_sort_array" );

                // sorting events are subscribed to multiple times for different uses cases
                expect( SEP.events.subscribe.calls.count() ).toEqual( 13 );
                
                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "allFiles" , jasmine.any( Function ) );
                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "downloadedFiles" , jasmine.any( Function ) );
                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "downloadQueue" , jasmine.any( Function ) );

                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "title" , jasmine.any( Function ) );
                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "type" , jasmine.any( Function ) );
                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "lastModify" , jasmine.any( Function ) );
                expect( SEP.events.subscribe ).toHaveBeenCalledWith( "size" , jasmine.any( Function ) );
            } );
            it( "will only register events using SEP.events.subscribe once" , function() {
                SEP.events.subscribe.calls.reset();
                userSettings.getSetting.calls.reset();
                var collection2 = new SEP.RegisterCollectionView( collectionOverlayId , { viewType: "allFiles" } );

                expect( SEP.events.subscribe ).not.toHaveBeenCalled();
                expect( userSettings.getSetting ).not.toHaveBeenCalled();
            } );
            it( "is a function returning an Object of shelf methods" , function() {
                expect( collection ).toEqual( jasmine.any( Object ) );
            } );
        } );

        describe( "Sorting" , function() {
            /*
                TO DO:
                    1. Add default sort ascending/descending values. Ie., lastModify is false whereas all others are true
            */

            describe( "can be loaded with a specific sort type, \"setSort\"" , function() {
                var tempUpdateTranslationForString = SEP.updateTranslationForString,
                    originalSMTimout = SM.setTimeout,
                    synchronousSetTimeout = function() {
                        return function( fn , timeout ) {
                            // Run asynchronous functions synchronously
                            fn();
                        };
                    };

                beforeEach( function() {
                    spyOn( SM , "runAction" );
                    spyOn( SEP.events , "publish" ).and.callThrough();
                    spyOn( userSettings , "setSetting" ).and.callThrough();

                    spyOn( SM , "setText" );
                    spyOn( SM , "moveOverlay" );
                    SEP.updateTranslationForString = function( string ) { return string; };
                
                    collection = new SEP.RegisterCollectionView( collectionOverlayId , { viewType: "allFiles" } );
                } );
                afterEach( function() {
                    SEP.updateTranslationForString = tempUpdateTranslationForString;
                    SM.setTimeout = originalSMTimout;
                    collection.clear();
                } );
                it( "uses SEP.events.publish" , function( done ) {
                    // Not sure if this is the best idea but timeout is forced to be synchronous
                    // Not sure how to apply Jasmine asynchronous support to timeout
                    SM.setTimeout = synchronousSetTimeout();
                    collection.setSort( "title" );
                    
                    expect( SEP.events.publish ).toHaveBeenCalledWith( "title" , jasmine.any( Object ) );
                    expect( userSettings.setSetting ).toHaveBeenCalledWith( "sep_sort_array" , jasmine.any( String ) );
                    done();
                } );
                it( "calls sortItems action" , function() {
                    // Defaul font is ascending, this call will result in descending alphabetical
                    collection.setSort( "title" );
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "sortItems",
                        trigger: "now",
                        targets: [ collectionOverlayId ],
                        data: jasmine.objectContaining( {
                            sortUsing: "title",
                            ascending: false
                        } )
                    } ) );
                } );
                it( "calls setDefaultSort action" , function() {
                    // Defaul font is ascending, this call will result in descending alphabetical
                    collection.setSort( "title" );
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "setDefaultSort",
                        trigger: "now",
                        target: "#systemPackageDataRegistry",
                        data: jasmine.objectContaining( {
                            sortUsing: "title",
                            ascending: false
                        } )
                    } ) );
                } );

                describe( "Options panel" , function() {
                    it( "will update the options panel sort text" , function() {
                        SM.setText.calls.reset();

                        // Defaul font is ascending, this call will result in descending alphabetical
                        collection.setSort( "title" );
                        expect( SM.setText.calls.count() ).toEqual( 1 );
                        expect( SM.setText ).toHaveBeenCalledWith( "sortType_btn_name_myFiles" , "        Z - A" );

                        collection.setSort( "title" );
                        expect( SM.setText ).toHaveBeenCalledWith( "sortType_btn_name_myFiles" , "        A - Z" );

                    } );
                    it( "will update the options panel current and previous sort text" , function() {
                        collection.setSort( "lastModify" );
                        // Both previous and current text is set
                        expect( SM.setText.calls.count() ).toEqual( 2 );
                        expect( SM.setText ).toHaveBeenCalledWith( "sortType_btn_date_myFiles" , "        Newest - Oldest" );
                        expect( SM.setText ).toHaveBeenCalledWith( "sortType_btn_name_myFiles" , "        A - Z" );
                    
                        collection.setSort( "lastModify" );
                        expect( SM.setText ).toHaveBeenCalledWith( "sortType_btn_date_myFiles" , "        Oldest - Newest" );
                    } );
                    it( "will update the options panel checkmark position" , function() {
                        // Move checkmark for lastModify
                        SM.moveOverlay.calls.reset();
                        SM.runAction.calls.reset();
                        collection.setSort( "lastModify" );
                        expect( SM.moveOverlay ).toHaveBeenCalledWith( "sep_settings_checkmark_sort" , "30px" , "112px" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setHiddenAction",
                            trigger: "now",
                            targets: [ "sep_settings_checkmark_sort" ],
                            data: jasmine.objectContaining( {
                                hidden: false
                            } )
                        } ) );
                        
                        // Due to platform issues, maybe, "setToggleButtonAction" has been swapped with "toggleButtonOn"
                        // expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        //     action: "setToggleButtonAction",
                        //     trigger: "now",
                        //     target: "sortType_btn_date_myFiles",
                        //     data: jasmine.objectContaining( {
                        //         state: "on"
                        //     } )
                        // } ) );
                        
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "toggleButtonOn",
                            trigger: "now",
                            target: "sortType_btn_date_myFiles"
                        } ) );

                        // Move checkmark for title
                        SM.moveOverlay.calls.reset();
                        SM.runAction.calls.reset();
                        collection.setSort( "title" );
                        expect( SM.moveOverlay ).toHaveBeenCalledWith( "sep_settings_checkmark_sort" , "30px" , "72px" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setHiddenAction",
                            trigger: "now",
                            targets: [ "sep_settings_checkmark_sort" ],
                            data: jasmine.objectContaining( {
                                hidden: false
                            } )
                        } ) );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setToggleButtonAction",
                            trigger: "now",
                            target: "sortType_btn_name_myFiles",
                            data: jasmine.objectContaining( {
                                state: "on"
                            } )
                        } ) );
                    } );
                } );
                describe( "list view" , function() {
                    it( "will update the list view arrow" , function() {
                        // Reset
                        collection.setSort( "lastModify" );
                        SM.runAction.calls.reset();

                        collection.setSort( "title" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setHiddenAction",
                            trigger: "now",
                            targets: [ "sep_sort_btn_arrow_name" ],
                            data: jasmine.objectContaining( {
                                hidden: false
                            } )
                        } ) );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            trigger: "now",
                            target: "sep_sort_btn_arrow_name",
                            data: jasmine.objectContaining( {
                                text: "Up_arrow"
                            } )
                        } ) );

                        // Toggle text
                        collection.setSort( "title" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            trigger: "now",
                            target: "sep_sort_btn_arrow_name",
                            data: jasmine.objectContaining( {
                                text: "Down_arrow"
                            } )
                        } ) );
                    } );
                    it( "will update the list view arrow current and previous sort text" , function() {
                        collection.setSort( "lastModify" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setHiddenAction",
                            trigger: "now",
                            targets: [ "sep_sort_btn_arrow_date" ],
                            data: jasmine.objectContaining( {
                                hidden: false
                            } )
                        } ) );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setHiddenAction",
                            trigger: "now",
                            targets: [ "sep_sort_btn_arrow_name" ],
                            data: jasmine.objectContaining( {
                                hidden: true
                            } )
                        } ) );

                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            trigger: "now",
                            target: "sep_sort_btn_arrow_date",
                            data: jasmine.objectContaining( {
                                text: "Up_arrow"
                            } )
                        } ) );
                    } );
                    it( "will update list view for \"title\", \"type\", \"lastModify\", \"size\"" , function() {
                        collection.setSort( "size" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            target: "sep_sort_btn_arrow_size"
                        } ) );

                        collection.setSort( "lastModify" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            target: "sep_sort_btn_arrow_date"
                        } ) );

                        collection.setSort( "type" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            target: "sep_sort_btn_arrow_kind"
                        } ) );

                        collection.setSort( "title" );
                        expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                            action: "setTextAction",
                            target: "sep_sort_btn_arrow_name"
                        } ) );
                    } );
                } );
            } );

            // Requires sort to be "title" going into this test case
            // This should be cleaned up
            describe( "can be loaded without parameters to load the cached value, \"loadSortAction\"" , function() {
                var tempUpdateTranslationForString = SEP.updateTranslationForString;
                beforeEach( function() {
                    spyOn( SM , "runAction" );
                    spyOn( SEP.events , "publish" ).and.callThrough();
                    spyOn( userSettings , "setSetting" ).and.callThrough();
                    SEP.updateTranslationForString = function( string ) { return string; };

                    collection = new SEP.RegisterCollectionView( collectionOverlayId , { viewType: "allFiles" } );
                } );
                afterEach( function() {
                    SEP.updateTranslationForString = tempUpdateTranslationForString;
                    collection.clear();
                } );

                it( "uses setSort" , function() {
                    collection.loadSortAction();
                    // setSort publishs a sort type event
                    expect( SEP.events.publish ).toHaveBeenCalledWith( "title" , jasmine.any( Object ) );
                } );
                it( "loads the last cached value" , function() {
                    // Set up sort with lastModify
                    // lastModify will be ascending value is false
                    collection.setSort( "lastModify" );
                    SEP.events.publish.calls.reset();
                    
                    // With calls reset, load cached sort
                    // lastModify will be ascending value is false
                    collection.loadSortAction();
                    expect( SEP.events.publish ).toHaveBeenCalledWith( "lastModify" , jasmine.any( Object ) );
                } );
                it( "calls setDefaultSort action" , function() {
                    // lastModify will be ascending value is false
                    collection.setSort( "lastModify" );

                    SM.runAction.calls.reset();
                    collection.loadSortAction();

                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "setDefaultSort",
                        trigger: "now",
                        target: "#systemPackageDataRegistry",
                        data: jasmine.objectContaining( {
                            sortUsing: "lastModify",
                            ascending: false
                        } )
                    } ) );

                    // loadSortAction will not toggle the sort
                    collection.loadSortAction();

                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        data: jasmine.objectContaining( {
                            ascending: false
                        } )
                    } ) );
                } );
                it( "this should include the same test cases as setSort" , function() {
                    pending();
                } );
            } );
        } );

        describe( "View" , function() {
            var tempUpdateTranslationForString = SEP.updateTranslationForString;
            describe( "can be loaded with a specific view type" , function() {
                var originalSMTimout = SM.setTimeout,
                    synchronousSetTimeout = function() {
                        return function( fn , timeout ) {
                            // Run asynchronous functions synchronously
                            fn();
                        };
                    };
                beforeEach( function() {
                    spyOn( SM , "runAction" );
                    spyOn( SM , "getActivePageId" );
                    spyOn( SM , "setText" ).and.callThrough();
                    spyOn( userSettings , "setSetting" ).and.callThrough();
                    SEP.updateTranslationForString = function( string ) { return string; };

                    collection = new SEP.RegisterCollectionView( collectionOverlayId , { viewType: "allFiles" } );
                } );
                afterEach( function() {
                    collection.clear();
                    SM.setTimeout = originalSMTimout;
                    SEP.updateTranslationForString = tempUpdateTranslationForString;
                } );

                // Can't figure out how to apply Jasmine asynhronous support
                it( "saves view to persistent memory" , function() {
                    // Not sure if this is the best idea but timeout is forced to be synchronous
                    // Not sure how to apply Jasmine asynchronous support to timeout
                    SM.setTimeout = synchronousSetTimeout();
                    collection.setView( "allFiles" );
                    expect( SM.getActivePageId ).toHaveBeenCalled();
                    expect( userSettings.setSetting ).toHaveBeenCalledWith( "sep_view_history_array" , jasmine.any( String ) );
                } );

                // Can't figure out how to apply Jasmine asynhronous support
                it( "sets the view title text" , function() {
                    // Not sure if this is the best idea but timeout is forced to be synchronous
                    // Not sure how to apply Jasmine asynchronous support to timeout
                    SM.setTimeout = synchronousSetTimeout();
                    collection.setView( "allFiles" );

                    expect( SM.setText ).toHaveBeenCalledWith( "collectionView_title" , "MobileLibrary" );

                    // This probably isn't a great idea when using plugins. The actual calls to SM.runAction may be unknow
                    // expect( SM.runAction.calls.count() ).toEqual( 3 );
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "setHiddenAction",
                        trigger: "now",
                        targets: [ "collectionView_title" , "options_gearBtn" ],
                        data: jasmine.objectContaining( {
                            hidden: false
                        } )
                    } ) );
                } );
                it( "will hide the back button when at root folder" , function() {
                    collection.setView( "allFiles" );

                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "loadDataAction",
                        trigger: "now",
                        targets: [ collectionOverlayId ],
                        data: jasmine.objectContaining( {
                            returnNonHierarchicalData: false,
                            filters: {}
                        } )
                    } ) );
                } );
            } );
            describe( "can be loaded without parameters to load the cached value" , function() {
                it( "" , function() {
                    pending();
                } );
            } );
        } );

        describe( "Spawn package modal" , function() {
            it( "supports a single tap" , function() {
                pending();
            } );

            it( "supports a long single tap" , function() {
                pending();
            } );
        } );

        describe( "Set current package state" , function() {
            it( "is a convenience method for \"SEP.setcurrentPackageState\"" , function() {
                pending();
            } );
        } );
    } );

    describe( "iPad options panel, \"SEP.optionsPanelDisplay\"" , function() {
        it( "" , function() {
            pending();
        } );
    } );

    describe( "Plugin passive alert" , function() {
        beforeEach( function() {
            spyOn( SM , "runAction" );
            // Required if plugin is exposed
            spyOn( SM , "moveOverlay" ).and.callThrough();
        } );
        describe( "spawn alert, \"SEP.spawnPassiveMessage\"" , function() {
            it( "takes a plugin id and data to pass to the alert plugin" , function() {
                var pluginId = "alert_pluginId",
                    data = {};

                SEP.spawnPassiveMessage( pluginId , data );

                expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                    action: "displayPassiveMessageAction",
                    trigger: "now",
                    target: pluginId,
                    data: jasmine.any( Object )
                } ) );
            } );
            
            describe( "spawns text and image" , function() {
                var pluginId = "alert_pluginId",
                    data = {
                        fontColor: "#FF0000",
                        size: "1em",
                        text: "A message on the wire",
                        image: "image.png"
                    };
                beforeEach( function() {
                    SEP.spawnPassiveMessage( pluginId , data );
                } );

                it( "text styling can be font color" , function() {
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "displayPassiveMessageAction",
                        data: jasmine.objectContaining( {
                            fontColor: "#FF0000"
                        } )
                    } ) );
                } );
                it( "text styling can be text size" , function() {
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "displayPassiveMessageAction",
                        data: jasmine.objectContaining( {
                            size: "1em"
                        } )
                    } ) );
                } );
                it( "displays text" , function() {
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "displayPassiveMessageAction",
                        data: jasmine.objectContaining( {
                            text: "A message on the wire"
                        } )
                    } ) );
                } );
                it( "displays image" , function() {
                    expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                        action: "displayPassiveMessageAction",
                        data: jasmine.objectContaining( {
                            image: "image.png"
                        } )
                    } ) );
                } );
            } );
            it( "will close a message before spawning a new message" , function() {
                // Not sure how to test plugins
                pending();
            } );
            it( "requires SM.moveOverlay to position the message properly, this is a workaround" , function() {
                // Not sure how to test plugins
                pending();
            } );
            describe( "supports some specific functionality" , function() {
                it( "can display text font color based on the page" , function() {
                    // Not sure how to test plugins
                    pending();

                    // SEP.spawnPassiveMessage( 'alert_plugin' , {
                    //     'Page_cloud_home': {
                    //         'fontColor': '#878787'
                    //     } , 
                    //     'size': '1.5em',
                    //     'text': SEP.updateTranslationForString( 'Nothing in this collection.' ),
                    //     'image': 'art_assets/iPhone_emptyCollection.png'
                    // } );
                } );
                it( "can display text based on the view" , function() {
                    // Not sure how to test plugins
                    pending();

                    // SEP.spawnPassiveMessage( 'alert_plugin' , {
                    //     'Page_cloud_home': {
                    //         'fontColor': '#878787'
                    //     },
                    //     'size': '1.5em',
                    //     'downloadQueue': {
                    //         'text': SEP.updateTranslationForString( '#(sepFilterResultEmptyDownloadQueue)' )
                    //     },
                    //     'downloadedFiles': {
                    //         'text': SEP.updateTranslationForString( '#(sepFilterResultEmptyLocalFiles)' )
                    //     } , 
                    //     'text': SEP.updateTranslationForString( 'Opps. Looks like we are missing something.' ),
                    //     'image': ''
                    // } );
                } );
            } );
        } );

        describe( "dismiss alert, \"SEP.dismissPassiveMessage\"" , function() {
            it( "clears a message" , function() {
                var pluginId = "alert_pluginId";

                SEP.dismissPassiveMessage( pluginId );

                expect( SM.runAction ).toHaveBeenCalledWith( jasmine.objectContaining( {
                    action: "dismissPassiveMessageAction",
                    trigger: "now",
                    target: pluginId,
                    data: jasmine.any( Object )
                } ) );
            } );
        } );
    } );

    describe( "User log in, \"SEP.userLogin\"" , function() {
        /*
            TO DO:
                1. SEP.login.startService
        */

        it( "will log the user in if there is internet connectivity" , function() {
            pending();
        } );
        it( "will prompt the user if there is not internet connectivity" , function() {
            pending();
        } );
    } );
    
    describe( "User log out, \"SEP.userLogout\"" , function() {
        it( "will log the user out if there is internet connectivity" , function() {
            pending();
        } );
        it( "will prompt the user if there is not internet connectivity" , function() {
            pending();
        } );
        it( "cannot be called more than once within 30 seconds" , function() {
            pending();
        } );
    } );

    describe( "Previous view, \"SEP.getPreviousView\"" , function() {
        it( "will provide the previous view and page" , function() {
            pending();
        } );
    } );

    describe( "Plugin localization, \"SEP.updateTranslationForString\"" , function() {
        beforeEach( function() {
            spyOn( SM , "getProperty" );
            SEP.updateTranslationForString( "I doubt this translation really exists" );
        } );

        it( "takes a string and maps it to the appropriate translated key for device localization" , function() {
            expect( SM.getProperty ).toHaveBeenCalledWith( "jsLocalizer" , jasmine.any( String ) );
        } );
    } );

    describe( "\"SEP.closePackageModal\"" , function() {
        pending();
    } );

    describe( "\"SEP.setcurrentPackageState\"" , function() {
        pending();
    } );

    describe( "\"SEP.loadPackage\"" , function() {
        pending();
    } );

    describe( "\"SEP.removePackage\"" , function() {
        pending();
    } );

    describe( "\"SEP.downloadPackage\"" , function() {
        pending();
    } );

    describe( "\"SEP.installUpdatePackage\"" , function() {
        pending();
    } );

    describe( "\"SEP.emailPackage\"" , function() {
        pending();
    } );

    describe( "\"SEP.network.deviceOffLineAction\"" , function() {
        pending();
    } );
} );